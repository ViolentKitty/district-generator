"""
Looks through geoJSON files for precinct boundaries and finds which precincts
neighbor each other. The results are written to a JSON file in the
precinct_information directory. The data source files for each state have
different information. Thus, the unique identifiers for each precinct are
different ("dp" for Hawaii, "OID_" for Missouri, "PCTKEY" for Texas). 
"""

import geopandas
import json
from os.path import join, dirname, realpath
from shapely.strtree import STRtree
from collections import defaultdict

islands = [
    (150071607,150071606),
    (150071503,150034501),
    (150031701,150091310),
    (150091309,150091307),
    (150091308,150091006),
    (150091102,150091306),
    (150010101,150091306),
]

def main():
    # GEOID10 is the uid for Hawaii geoJSON
    find_precinct_neighbors('hi', 'GEOID10')
    # GEOID10 is the uid for Missouri geoJSON
    find_precinct_neighbors('mo', 'GEOID10')
    # GEOID10 is the uid for the Texas geoJSON
    find_precinct_neighbors('tx', 'GEOID10')


def find_precinct_neighbors(state_code, uid):
    root = join(dirname(realpath(__file__)), '.')
    data_dir = join(root, 'data', 'precinct_information')
    geojson_file = join(root, '..', 'src', 'main', 'resources', 'static', 'data', 'precinct_information', '%s_precincts.geojson' % state_code)
    demographics_file = join(data_dir, 'demographics', 'processed', '%s_demographic.json' % state_code)
    neighbors_file = join(data_dir, 'neighbors', '%s.json' % state_code)
    print('Writing neighbors file for %s to %s' % (state_code, neighbors_file))

    with open(demographics_file) as f:
        demographics = json.load(f)

    state = geopandas.read_file(geojson_file)

    centroids_to_row = {}
    for idx, row in state.iterrows():
        centroids_to_row[row.geometry.centroid.wkt] = row

    neighbors = defaultdict(list)
    tree = STRtree(state.geometry)
    for idx, row in state.iterrows():
        precinct_dp = row[uid]
        for polygon in tree.query(row.geometry):
            if row.geometry.touches(polygon):
                neighbor_dp = centroids_to_row[polygon.centroid.wkt][uid]
                owner_demographic = demographics[precinct_dp]
                neighbor_demographic = demographics[neighbor_dp]
                ethnic_joinabilities = []
                for key in owner_demographic:
                    if key != "total":
                        try:
                            p_owner = owner_demographic[key]/owner_demographic["total"]
                            p_neighbor = neighbor_demographic[key]/neighbor_demographic["total"]
                            e_join = 1. - abs(p_owner - p_neighbor)
                        except ZeroDivisionError:
                            e_join = 1.
                        ethnic_joinabilities.append(e_join)

                demographic_joinability = sum(ethnic_joinabilities)/len(ethnic_joinabilities)

                owner_county = precinct_dp[2:5]
                neighbor_county = neighbor_dp[2:5]
                county_joinability = float(owner_county == neighbor_county)
                neighbors[precinct_dp].append({
                    "id": neighbor_dp,
                    "demographic_joinability": demographic_joinability,
                    "county_joinability": county_joinability,
                })

    if state_code == 'hi':
        for island in islands:
            precinct_dp = str(island[0])
            neighbor_dp = str(island[1])
            owner_demographic = demographics[precinct_dp]
            neighbor_demographic = demographics[neighbor_dp]
            ethnic_joinabilities = []
            for key in owner_demographic:
                if key != "total":
                    try:
                        p_owner = owner_demographic[key]/owner_demographic["total"]
                        p_neighbor = neighbor_demographic[key]/neighbor_demographic["total"]
                        e_join = 1. - abs(p_owner - p_neighbor)
                    except ZeroDivisionError:
                        e_join = 1.
                    ethnic_joinabilities.append(e_join)

            demographic_joinability = sum(ethnic_joinabilities)/len(ethnic_joinabilities)

            owner_county = precinct_dp[2:5]
            neighbor_county = neighbor_dp[2:5]
            county_joinability = float(owner_county == neighbor_county)
            neighbors[precinct_dp].append({
                "id": neighbor_dp,
                "demographic_joinability": demographic_joinability,
                "county_joinability": county_joinability,
            })
            neighbors[neighbor_dp].append({
                "id": precinct_dp,
                "demographic_joinability": demographic_joinability,
                "county_joinability": county_joinability,
            })

    with open(neighbors_file, 'w') as f:
        json.dump(neighbors, f, sort_keys=True, indent=4)


if __name__ == '__main__':
    main()
