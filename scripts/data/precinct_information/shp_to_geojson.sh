#!/bin/sh
echo "Converting Hawaii shp files..."
ogr2ogr -f GeoJSON hi_precincts.geojson shapefiles/hi/HI_final.shp

echo "Converting Texas shp files..."
ogr2ogr -f GeoJSON -t_srs crs:84 tx_precincts.geojson shapefiles/tx/tl_2012_48_vtd10.shp

echo "Converting Missouri shp files..."
ogr2ogr -f GeoJSON -t_srs crs:84 mo_precincts.geojson shapefiles/mo/mo_final.shp


echo "Moving geoJSON files to js..."

echo "hi_data = " > hi_precincts.js
cat hi_precincts.geojson >> hi_precincts.js

echo "tx_data = " > tx_precincts.js
cat tx_precincts.geojson >> tx_precincts.js

echo "mo_data = " > mo_precincts.js
cat mo_precincts.geojson >> mo_precincts.js


echo "Done!"
