"""
find_precinct_elections_tx.py

This script looks through the CSV file for election data, gathers the election
results for democratic and republican votes, and then outputs to a JSON file
associating a precinct ID to the election results.
"""
import json
import csv
from os.path import join, dirname, realpath
from collections import defaultdict


def main():
    root = join(dirname(realpath(__file__)), '.')
    data_dir = join(root, 'data', 'precinct_information')
    election_dir = join(data_dir, 'elections')
    elections_csv = join(election_dir, 'tx_election.csv')
    demographics_dir = join(data_dir, 'demographics', 'processed')
    demographics_file = join(demographics_dir, 'tx_demographic.json')
    json_file_path = join(election_dir, 'processed', 'tx_election.json')
    print('Writing elections file for TX data')

    elections = {}
    all_geoids = set()
    with open(elections_csv) as f:
        reader = csv.DictReader(f)
        for row in reader:
            geoid = row['cntyvtd']
            if len(row['fips']) == 1:
                geoid = '00' + geoid
            elif len(row['fips']) == 2:
                geoid = '0' + geoid
            geoid = '48' + geoid

            democrats = row['t_Gov_D_2010']
            republicans = row['t_Gov_R_2010']

            elections[geoid] = {
                "dem_votes": int(democrats),
                "rep_votes": int(republicans),
                "ind_votes": 0
            }
            all_geoids.add(geoid)

    with open(demographics_file) as f:
        demographics = json.load(f)
        all_texas_precincts = set(demographics.keys())

        missed_precincts = all_texas_precincts - all_geoids
        remove_precincts = set()
        for p in missed_precincts:
            suffix_char = 'A'
            elections[p] = defaultdict(int)
            while (p + suffix_char) in elections:
                for party in elections[p + suffix_char]:
                    elections[p][party] += elections[p + suffix_char][party]
                del elections[p + suffix_char]
                suffix_char = chr(ord(suffix_char) + 1)
            if suffix_char != 'A':
                remove_precincts.add(p)

        missed_precincts -= remove_precincts

        print("Missed the following precincts' election data:")
        for p in missed_precincts:
            print(p)

            total_pop = demographics[p]['total']
            election_rate = .5
            p_rep = .55
            p_dem = .42

            dem_votes = int(total_pop * election_rate * p_dem)
            rep_votes = int(total_pop * election_rate * p_rep)

            elections[p] = {
                "dem_votes": dem_votes,
                "rep_votes": rep_votes,
                "ind_votes": 0
            }


    with open(json_file_path, 'w') as f:
        json.dump(elections, f, sort_keys=True, indent=4)


if __name__ == '__main__':
    main()
