import json
import subprocess

precinct_insert_string = " INSERT INTO Precinct SET `id` = %d , `name` = \'%s\', `area` =  %f, `perimeter` =  %f, `state_id` = %d, `precinct_geometry_wkt` =  \'%s\' , `district_id` = 0, `election_id` = 0; \n"

def makeAllCharToInt(string):
    final_string = ""

    for char in string:
        if str(char).isdigit():
            final_string+=char
        else:
            final_string += str(ord(char))
    return final_string



def main():
    subprocess.call(['sh', '../precinct_information/shp_to_geojson.sh'])
    print("Compiling Hawaii Statements")
    hi_sql_string = "USE astros;\n DELETE FROM precinct WHERE state_id = 2; \n "
    hi_precincts_string = open("../data/hi_precincts.geojson", 'r').read()
    hi_precincts = json.loads(hi_precincts_string)
    feature_array = (hi_precincts['features'])
    num_precincts = 0
    for feature in feature_array:
        properties = feature['properties']
        unique_id = -1
        if not properties['GEOID10'].isdigit():
            unique_id = int(makeAllCharToInt(properties['GEOID10']))
        else:
            unique_id = int(properties['GEOID10'])
        state_id = 2 # self made default for hawaii
        area = properties['ALAND10'] +  + properties['AWATER10']
        perimeter = 0 # again no perimeter for hawaii :(
        name = str(properties['NAME10'].replace("\'", ""))
        poly_string = str(feature["geometry"]).replace("\'", "\"")

        sql_statement = (precinct_insert_string % (unique_id, name, area, perimeter, state_id, poly_string))
        hi_sql_string += (sql_statement)
        num_precincts += 1
        print("Precincts complete: "+ str(num_precincts), end = '\r')
    print()

    text_file = open("hi.sql", "w")
    text_file.write(hi_sql_string)
    text_file.close()
    print("Hawaii -> Done")


    print("Compiling Missouri Statements")
    mo_sql_string = "USE astros;\n DELETE FROM precinct WHERE state_id = 1; \n "
    mo_precincts_string = open("data/mo_precincts.geojson", 'r').read()
    mo_precincts = json.loads(mo_precincts_string)
    feature_array = (mo_precincts['features'])
    num_precincts = 0
    for feature in feature_array:

        properties = feature['properties']

        unique_id = -1
        if not properties['GEOID10'].isdigit():
            unique_id = int(makeAllCharToInt(properties['GEOID10']))
        else:
            unique_id = int(properties['GEOID10'])
        state_id = 1 # self made default for missouri
        area = properties['ALAND10'] + properties['AWATER10']
        perimeter = 0 # Missouri geo data does not have perimeter :(
        name = properties['NAMELSAD10'].replace("\'", "") # it seems hawaii precincts do not have names
        poly_string = str(feature["geometry"]).replace("\'", "\"")

        sql_statement = (precinct_insert_string % (unique_id, name, area, perimeter, state_id, poly_string))
        mo_sql_string += (sql_statement)
        num_precincts += 1
        print("Precincts complete: "+ str(num_precincts), end = '\r')
    print()

    text_file = open("mo.sql", "w")
    text_file.write(mo_sql_string)
    text_file.close()
    print("Missouri -> Done")


    print("Compiling Texas Statements")
    tx_sql_string = "USE astros;\n DELETE FROM precinct WHERE state_id = 0; \n "
    tx_precincts_string = open("data/tx_precincts.geojson", 'r').read()
    tx_precincts = json.loads(tx_precincts_string)
    feature_array = (tx_precincts['features'])
    num_precincts = 0
    for feature in feature_array:
        properties = feature['properties']
        unique_id = -1
        id_string = properties['GEOID10']
        if not id_string.isdigit():
            unique_id = int(makeAllCharToInt(id_string))
        else:
            unique_id = int(id_string)
        state_id = 0 # self made default for texas
        area = properties['ALAND10'] + properties['AWATER10']
        perimeter = 0 # needs to be computed
        name = str(properties['NAMELSAD10'].replace("\'", "")) 

        poly_string = str(feature["geometry"]).replace("\'", "\"")

        sql_statement = (precinct_insert_string % (unique_id, name, area, perimeter, state_id, poly_string))
        tx_sql_string += (sql_statement)
        num_precincts += 1
        print("Precincts complete: " + str(num_precincts), end = '\r')
    print()

    text_file = open("tx.sql", "w")
    text_file.write(tx_sql_string)
    text_file.close()
    print("Texas -> Done")




    return 0

if __name__ == '__main__':
    main()