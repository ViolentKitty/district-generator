import json


precinct_insert_string = "INSERT INTO election SET `election_id` = %d, `dem_votes` = %d , `ind_votes` = %d, `rep_votes` = %d, `precinct_id` = %d, `district_id` = -1; \n UPDATE Precinct p SET p.election_id = %d WHERE p.id = %d; \n"

def makeAllCharToInt(string):
    final_string = ""

    for char in string:
        if str(char).isdigit():
            final_string+=char
        else:
            final_string += str(ord(char))
    return int(final_string)


def main():
    
    make_sql_for_state("hi")
    make_sql_for_state("mo")
    make_sql_for_state("tx")

    return -1

id_num = 1
def make_sql_for_state(state):
    global id_num
    sql_string = "USE astros; \n DELETE FROM election; UPDATE precinct SET election_id = 0; \n"
    json_string = open("../data/precinct_information/elections/processed/"+str(state)+"_election.json", "r").read()
    json_obj = json.loads(json_string)
    
    precincts_complete = 0

    for key in json_obj.keys():    
        temp_elec = json_obj[key]

        unique_id = int(makeAllCharToInt(key))

        sql_string += (precinct_insert_string % (id_num, temp_elec['dem_votes'], temp_elec['ind_votes'], temp_elec['rep_votes'], unique_id, id_num, unique_id))
        id_num += 1

        precincts_complete += 1
        print("Precincts complete: "+ str(precincts_complete), end = '\r')
    text_file = open(str(state)+"_election_precinct_level.sql", "w")
    text_file.write(sql_string)
    text_file.close()
    print(str(state)+" -> Done")  
    return 0



if __name__ == '__main__':
    main()

