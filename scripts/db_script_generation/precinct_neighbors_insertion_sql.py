import json


precinct_insert_string = "INSERT INTO precinct_neighbor SET `owner_id` = %d , `neighbor_id` = %d, `county_joinability` = %f, `demographic_joinability` = %f; \n"

def makeAllCharToInt(string):
    final_string = ""

    for char in string:
        if str(char).isdigit():
            final_string+=char
        else:
            final_string += str(ord(char))
    return int(final_string)


def main():
    
    make_sql_for_state("hi")
    make_sql_for_state("mo")
    make_sql_for_state("tx")

    return -1

def make_sql_for_state(state):
    sql_string = "USE astros;\n DELETE FROM precinct_neighbors; \n"
    json_string = open("../data/precinct_information/neighbors/"+str(state)+".json", "r").read()
    json_obj = json.loads(json_string)
    
    precincts_complete = 0

    for key in json_obj.keys():
        neighbors = json_obj[key]
        for neighbor in neighbors:
            sql_string += (precinct_insert_string % (makeAllCharToInt(key), makeAllCharToInt(neighbor['id']), neighbor['county_joinability'], neighbor['demographic_joinability']))
        precincts_complete += 1
        print("Precincts complete: "+ str(precincts_complete), end = '\r')
    text_file = open(str(state)+"_neighbors.sql", "w")
    text_file.write(sql_string)
    text_file.close()
    print(str(state)+" -> Done")  
    return 0



if __name__ == '__main__':
    main()

