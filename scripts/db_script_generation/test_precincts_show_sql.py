from collections import deque
import json

SQL_INSERT_STATE_4 = "INSERT INTO state SET `id` = 4, `name` = 'test', `abbreviation` = 'tst', `area` = 0.0; \n"
SQL_UPDATE_BOILERPLATE = "UPDATE precinct SET state_id = 4 WHERE id = %d; \n"

def main():
    start_precinct_id_number = "484394057"
    precinct_neighbor_dataset_location = "../data/precinct_information/neighbors/tx.json"
    prec_neighbors = json.loads(open(precinct_neighbor_dataset_location, 'r').read())

    prec_array = bfs_till_n_nodes(prec_neighbors, start_id = start_precinct_id_number, n = 1000)

    text_file = open("../data/precinct_information/test_precincts.json", "w")
    text_file.write(json.dumps(prec_array))
    text_file.close()

    sql_string = compile_sql_statements_for_precincts(prec_array)
    sql_file = open("insert_test_state.sql", "w")
    sql_file.write(sql_string)
    sql_file.close()
    
    return 0

def compile_sql_statements_for_precincts(precincts):
    sql_string = "USE Astros; \n"
    sql_string += SQL_INSERT_STATE_4
    
    for precinct in precincts:
        sql_string += (SQL_UPDATE_BOILERPLATE % precinct)

    return sql_string


def bfs_till_n_nodes(prec_neighbors, start_id, n):
    prec_array = []
    queue = deque()
    queue.append(str(start_id))
    while len(queue) > 0 and len(prec_array) < n:
        node = int(queue.popleft())
        prec_array.append(node)
        for neighbor in prec_neighbors[str(node)]:
            queue.append(neighbor)
    return prec_array




if __name__ == '__main__':
    main()