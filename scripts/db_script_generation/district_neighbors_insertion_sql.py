import json


disneighbor_insert_string = "INSERT INTO district_neighbor SET `id` = %d, `owner_id` = %d , `neighbor_id` = %d; \n"

def main():
    
    sql_string = "USE astros; \n DELETE FROM district_neighbor; \n"
    string = open("../data/districts.json", "r").read()
    json_obj = json.loads(string)
    json_ids = json.loads(open("../data/district_id_mapping.json", "r").read())
    
    precincts_complete = 0
    unique_id = 1
    for district in json_obj.keys():    
        
        owner_id = json_ids[district]

        neighbors = json_obj[district]['neighbors']
        for neighbor in neighbors:
            neighbor_id = json_ids[neighbor]
            sql_string += (disneighbor_insert_string % (unique_id, owner_id, neighbor_id))
            unique_id += 1

        precincts_complete += 1
        print("Districts complete: "+ str(precincts_complete))

    
    text_file = open("district_neighbors.sql", "w")
    text_file.write(sql_string)
    text_file.close()
    print(" -> Done")  
    

    return 0



if __name__ == '__main__':
    main()