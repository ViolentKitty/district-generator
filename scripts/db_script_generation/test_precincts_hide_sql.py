from collections import deque
import json

SQL_DELETE_STATE_4 = "DELETE FROM state WHERE id = 4; \n"
SQL_UPDATE_BOILERPLATE = "UPDATE precinct SET state_id = 0 WHERE id = %d; \n"

def main():

    test_precincts_dataset_location = "../data/precinct_information/test_precincts.json"
    test_precincts = json.loads(open(test_precincts_dataset_location, 'r').read())
    

    sql_string = " USE Astros; \n"

    for precinct in test_precincts:
        sql_string += (SQL_UPDATE_BOILERPLATE % precinct)
    sql_string += SQL_DELETE_STATE_4

    sql_file = open("remove_test_state.sql", "w")
    sql_file.write(sql_string)
    sql_file.close()
    return 0


if __name__ == '__main__':
    main()