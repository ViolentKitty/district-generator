import json


precinct_insert_string = "INSERT INTO demographic SET `demographic_id` = %d, `african_american` = %d , `aian` = %d, `asian` = %d, `caucasian` = %d, `hispanic` = %d, `other` = %d, `nhopi` = %d, `total` = %d, `precinct_id` = %d, `district_id` = -1; \n UPDATE Precinct p SET p.demographic_id = %d WHERE p.id = %d; \n"

def makeAllCharToInt(string):
    final_string = ""

    for char in string:
        if str(char).isdigit():
            final_string+=char
        else:
            final_string += str(ord(char))
    return int(final_string)


def main():
    
    make_sql_for_state("hi")
    make_sql_for_state("mo")
    make_sql_for_state("tx")

    return -1

id_num = 1
def make_sql_for_state(state):
    global id_num
    sql_string = "USE astros;\n DELETE FROM demographic; UPDATE precinct SET demographic_id = 0 WHERE demographic_id != -1; \n "
    json_string = open("../data/precinct_information/demographics/processed/"+str(state)+"_demographic.json", "r").read()
    json_obj = json.loads(json_string)
    
    precincts_complete = 0

    for key in json_obj.keys():    
        temp_demo = json_obj[key]

        unique_id = int(makeAllCharToInt(key))

        sql_string += (precinct_insert_string % (id_num, temp_demo['black'], temp_demo['aian'], temp_demo['asian'], temp_demo['white'], temp_demo['hispanic'], temp_demo['other'], temp_demo['nhopi'], temp_demo['total'], unique_id, id_num, unique_id))
        id_num += 1

        precincts_complete += 1
        print("Precincts complete: "+ str(precincts_complete), end = '\r')
    text_file = open(str(state)+"_demographic_precinct_level.sql", "w")
    text_file.write(sql_string)
    text_file.close()
    print(str(state)+" -> Done")  
    return 0



if __name__ == '__main__':
    main()

