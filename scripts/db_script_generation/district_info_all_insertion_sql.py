import json

district_insert_statement = "INSERT INTO district SET `id` = %d, `state_id` = %d , `name` = \'%s\', `area` = %f; \n"
district_demo_insert_statement = "INSERT INTO demographic  SET `demographic_id` = %d, `african_american` = %d , `aian` = %d, `asian` = %d, `caucasian` = %d, `hispanic` = %d, `other` = %d, `nhopi` = %d, `total` = %d, `precinct_id` = -1, `district_id` = %d; \n UPDATE district p SET p.demographic_id = %d WHERE p.id = %d; \n"
district_elec_insert_statement = "INSERT INTO election SET `election_id` = %d, `dem_votes` = %d , `ind_votes` = %d, `rep_votes` = %d, `precinct_id` = -1, `district_id` = %d; \n UPDATE District p SET p.election_id = %d WHERE p.id = %d; \n"
precinct_update_statement = "UPDATE Precinct p SET p.district_id = %d WHERE p.id = %d ; \n"

def makeAllCharToInt(string):
    final_string = ""

    for char in string:
        if str(char).isdigit():
            final_string+=char
        else:
            final_string += str(ord(char))
    return int(final_string)


def main():
    district_id_num = 1
    district_demo_id_number = 100001
    sql_string = "USE Astros; \n DELETE FROM district; UPDATE precinct SET district_id = 0; \n "
    json_string = open("../data/districts.json", "r").read()
    json_obj = json.loads(json_string)

    for key in json_obj.keys():
        temp_demo = json_obj[key]["demographic"]
        temp_elec = json_obj[key]["election"]
        state_id = -1

        if key[0] == 'T':
            state_id = 0
        if key[0] == 'M':
            state_id = 1
        if key[0] == 'H':
            state_id = 2

        sql_string += (district_insert_statement % (district_id_num, state_id, key, json_obj[key]['area']))
        sql_string += (district_demo_insert_statement % (district_demo_id_number, temp_demo['black'], temp_demo['aian'], temp_demo['asian'], temp_demo['white'], temp_demo['hispanic'], temp_demo['other'], temp_demo['nhopi'], temp_demo['total'], district_id_num, district_demo_id_number, district_id_num))
        sql_string += (district_elec_insert_statement % (district_demo_id_number, temp_elec['dem_votes'], temp_elec['ind_votes'], temp_elec['rep_votes'], district_id_num, district_demo_id_number, district_id_num))
        
        
        precincts =  json_obj[key]["precincts"]

        for precinct in precincts:
            precinct_id = makeAllCharToInt(precinct)
            sql_string += (precinct_update_statement %(district_id_num, precinct_id))


        district_id_num += 1
        district_demo_id_number += 1
        print(district_id_num)
        
    text_file = open("default_district_information.sql", "w")
    text_file.write(sql_string)
    text_file.close()
    print("done")

    return 0

if __name__ == "__main__":
    main()