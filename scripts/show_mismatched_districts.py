"""
show_mismatched_districts.py
"""

import pandas
import geopandas
import json
import sys
from shapely.strtree import STRtree
from os.path import join, dirname, realpath
from collections import defaultdict
import matplotlib.pyplot as plt


DISTRICT_COUNTS = {
    'HI' : 2,
    'MO' : 8,
    'TX' : 36
}

STATE_CODES = {
    'TX' : 48,
    'HI' : 15,
    'MO' : 29,
}

def main():
    global STATE_CODES

    root = join(dirname(realpath(__file__)), '.')
    data_dir = join(root, 'data')
    precinct_dir = join(data_dir, 'precinct_information')
    districts_file_path = join(data_dir, 'districts.json')
    with open(districts_file_path) as f:
        districts = json.load(f)

    all_p = []
    for d in districts.values():
        for p in d['precincts']:
            all_p.append(p)

    for code in STATE_CODES:
        districts_geojson = get_districts_geojson(code)
        ax = districts_geojson.plot(facecolor='white', edgecolor='red')

        geojson_file_path = join(precinct_dir, '%s_precincts.geojson' % code)
        precincts = geopandas.read_file(geojson_file_path)

        for idx, row in precincts.iterrows():
            geoid = row['GEOID10']
            if str(geoid) not in all_p:
                geopandas.GeoSeries(row).plot(ax=ax)
        plt.show()


def get_districts_geojson(state_code):
    district_count = DISTRICT_COUNTS[state_code]
    all_districts = []
    for i in range(1, district_count + 1):
        geojson_file_url = "https://theunitedstates.io/districts/cds/2016/%s-%d/shape.geojson" % (state_code, i)
        district_df = geopandas.read_file(geojson_file_url)
        copy_df = district_df.copy()
        copy_df = copy_df.to_crs({'init': 'epsg:3857'})
        district_df['area'] = copy_df['geometry'].area
        all_districts.append(district_df)
    return geopandas.GeoDataFrame(pandas.concat(all_districts))

if __name__ == '__main__':
    main()
