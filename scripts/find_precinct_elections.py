"""
find_precinct_elections.py

This script looks through the CSV file for election data, gathers the election
results for democratic and republican votes, and then outputs to a JSON file
associating a precinct ID to the election results.
"""
import json
import csv
import geopandas
from os.path import join, dirname, realpath

def main():
    elections('hi', 'GEOID10', 'NRV', 'NDV')
    elections('mo', 'GEOID10', 'PR_DV08', 'PR_RV08', 'PR_OTHV08')

def elections(state_code, uid, d_key, r_key, i_key=None):
    root = join(dirname(realpath(__file__)), '.')
    data_dir = join(root, 'data', 'precinct_information')
    geojson_file = join(data_dir, '%s_precincts.geojson' % state_code)
    election_dir = join(data_dir, 'elections')
    json_file_path = join(election_dir, 'processed', '%s_election.json' % state_code)
    print('Writing elections file for %s data to %s' % (state_code, json_file_path))

    state = geopandas.read_file(geojson_file)
    elections = {}
    for idx, row in state.iterrows():
        identifier = row[uid]
        democrats = row[d_key]
        republicans = row[r_key]
        independents = row[i_key] if i_key is not None else 0
        elections[identifier] = {
            "dem_votes": int(democrats),
            "rep_votes": int(republicans), 
            "ind_votes": int(independents)
        }

    with open(json_file_path, 'w') as f:
        json.dump(elections, f, sort_keys=True, indent=4)


if __name__ == '__main__':
    main()
