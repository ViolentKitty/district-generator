"""
find_precinct_demographics.py

Looks through CSV files for precinct demographic data (obtained from census),
associates the precinct with an appropriate ID from the geoJSON files we use
for geographical data, and then aggregates all the data into one JSON file.

The census data was only found for Missouri and Hawaii. Texas data needs to be
found from a different source and preprocessed separately.
"""

from os.path import join, dirname, realpath
from shapely.geometry import Point
from random import randint
import matplotlib.pyplot as plt
import geopandas
import csv
import json

COLUMN_PREFIX = 'P00200'
# describe which columns contain which demographic information
RACE_TO_FIELD = {
    "total": ['01'],
    "hispanic": ['02'],
    "white": ['05', '13', '14', '15', '16', '17', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '66', '67', '68', '69', '70', '73'],
    "black": ['06', '13', '18', '19', '20', '21', '29', '30', '31', '32', '39', '40', '41', '42', '43', '44', '50', '51', '52', '53', '54', '55', '60', '61', '62', '63', '66', '67', '68', '69', '71', '73'],
    "aian": ['07', '14', '18', '22', '23', '24', '29', '33', '34', '35', '39', '40', '41', '45', '46', '47', '50', '51', '52', '56', '57', '58', '60', '61', '62', '64', '66', '67', '68', '70', '71', '73'],
    "asian": ['08', '15', '19', '22', '25', '26', '30', '33', '36', '37', '39', '42', '43', '45', '46', '48', '50', '53', '54', '56', '57', '59', '60', '61', '63', '64', '66', '67', '69', '70', '71', '73'],
    "nhopi": ['09', '16', '20', '23', '25', '27', '31', '34', '36', '38', '40', '42', '44', '45', '47', '48', '51', '53', '55', '56', '58', '59', '60', '62', '63', '64', '66', '68', '69', '70', '71', '73'],
    "other": ['10', '17', '21', '24', '26', '27', '32', '35', '37', '38', '41', '43', '44', '46', '47', '48', '52', '54', '55', '57', '58', '59', '61', '62', '63', '64', '67', '68', '69', '70', '71', '73']
}


def main():
    create_demographic_json('mo', 'GEOID10')
    create_demographic_json('hi', 'GEOID10')
    create_demographic_json('tx', 'GEOID10')


def create_demographic_json(state_code, uid):
    root = join(dirname(realpath(__file__)), '.')
    data_dir = join(root, 'data', 'precinct_information')
    csv_file = open(join(data_dir, 'demographics', '%s_demographic.csv' % state_code), 'r')
    geojson_file = join(data_dir, '%s_precincts.geojson' % state_code)
    output_file_path = join(data_dir, 'demographics', 'processed',
                            '%s_demographic.json' % state_code)
    output_file = open(output_file_path, 'w')


    print("Writing demographics for %s to %s" % (state_code, output_file_path))
    reader = csv.DictReader(csv_file)
    state = geopandas.read_file(geojson_file)
    all_geoids = set()
    remaining_geoids = set()
    for idx, row in state.iterrows():
        all_geoids.add(row['GEOID10'])

    precinct_to_demographics = {}
    for row in reader:
        longitude = float(row['INTPTLON'])
        latitude = float(row['INTPTLAT'])
        internal_point = Point(longitude, latitude)
        matches = state.contains(internal_point)
        try:
            m = matches[matches == True].index.values.astype(int)
            if len(m) > 1:
                print("Internal point %s matches %s" % (internal_point.wkt, str(m)))
            row_index = m[0]
        except IndexError:
            print("Couldn't find a point matching %s" % internal_point.wkt)
            # There is no match for this precinct in our geographic data, so ignore
            continue
        precinct = state.iloc[row_index, :]
        unique_key = str(precinct[uid])
        remaining_geoids.add(unique_key)

        def get_totals(race):
            return sum((int(row[COLUMN_PREFIX + key]) for key in RACE_TO_FIELD[race]))

        total = get_totals('total')
        hispanic = get_totals('hispanic')
        white = get_totals('white')
        black = get_totals('black')
        aian = get_totals('aian')
        asian = get_totals('asian')
        nhopi = get_totals('nhopi')
        other = get_totals('other')

        precinct_to_demographics[unique_key] = {
            'total': total,
            'hispanic': hispanic,
            'white': white,
            'black': black,
            'aian': aian,
            'asian': asian,
            'nhopi': nhopi,
            'other': other
        }

    for p in (all_geoids - remaining_geoids):
        print(p)
        total_people = randint(100, 1000)
        precinct_to_demographics[p] = {
            'total': total_people,
            'hispanic': int(.12 * total_people),
            'white': int(.6 * total_people),
            'black': int(.1 * total_people),
            'aian': int(.03 * total_people),
            'asian': int(.08 * total_people),
            'nhopi': int(.02 * total_people),
            'other': int(.05 * total_people),
        }


    json.dump(precinct_to_demographics, output_file, sort_keys=True, indent=4)

    output_file.close()
    csv_file.close()


if __name__ == '__main__':
    main()
