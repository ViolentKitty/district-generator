"""

get_initial_districts.py

This script gathers the required data for the initial districts on the map.
This information includes a district id, the area of the district, all the
neighboring districts of the district, all precincts contained in the district,
and the demographic and election data of all the precincts in the district
combined. This information is deposited in a JSON file. 

"""

import pandas
import geopandas
import json
import sys
from shapely.strtree import STRtree
from os.path import join, dirname, realpath
from collections import defaultdict

INTERSECTION_THRESHOLD = .51

DISTRICT_COUNTS = {
    'HI' : 2,
    'MO' : 8,
    'TX' : 36
}

STATE_CODES = {
    'TX' : 48,
    'HI' : 15,
    'MO' : 29,
}

def main():
    write_districts_json()

def write_districts_json():
    root = join(dirname(realpath(__file__)), '.')
    data_dir = join(root, 'data')
    precinct_dir = join(data_dir, 'precinct_information')
    
    all_district_data = {}
    for state_code in STATE_CODES:
        state_districts_df = get_districts_geojson(state_code)
        # get id, area, neighbors, internal precincts, internal demographics, internal election data
        geojson_file_path = join(precinct_dir, 
                                 "%s_precincts.geojson" % state_code.lower())
        precincts = geopandas.read_file(geojson_file_path)

        election_file_path = join(precinct_dir,
                                  'elections',
                                  'processed',
                                  '%s_election.json' % state_code.lower())
        election_f = open(election_file_path)
        elections = json.load(election_f)

        demographic_file_path = join(precinct_dir,
                                     'demographics',
                                     'processed',
                                     '%s_demographic.json' % state_code.lower())
        demographic_f = open(demographic_file_path)
        demographics = json.load(demographic_f)
        precinct_tree = STRtree(precincts.geometry)
        centroids_to_row = {}
        district_tree = STRtree(state_districts_df.geometry)
        districts_to_rows = {}
        for idx, row in precincts.iterrows():
            centroids_to_row[row['geometry'].centroid.wkt] = row
        for idx, row in state_districts_df.iterrows():
            districts_to_rows[row['geometry'].centroid.wkt] = row
        districts_to_precincts = get_internal_precincts(precincts, district_tree, districts_to_rows)
        print("Found %d total precincts for %s" % (sum(len(d) for d in districts_to_precincts.values()), state_code))
        for idx, district in state_districts_df.iterrows():
            district_id = district['Code']
            area = district['area']

            print("Getting precincts of %s" % district_id)
            precinct_ids = districts_to_precincts[district['Code']]

            print("Getting election data of %s" % district_id)
            election_data = get_district_election_data(precinct_ids, elections)

            print("Getting demographic data of %s" % district_id)
            demographic_data = get_district_demographic_data(precinct_ids, demographics)

            print("Getting neighbor data of %s" % district_id)
            neighbor_data = get_district_neighbors(district, state_districts_df, district_tree, districts_to_rows)

            district_obj = {
                'area': area,
                'precincts': precinct_ids,
                'election' : election_data,
                'demographic' : demographic_data,
                'neighbors': neighbor_data
            }
            all_district_data[district_id] = district_obj

        election_f.close()
        demographic_f.close()

    json_file_path = join(data_dir, 'districts.json')
    with open(json_file_path, 'w') as f:
        json.dump(all_district_data, f, sort_keys=True, indent=4)

def get_internal_precincts(precincts, district_tree, districts_to_rows):
    district_to_precincts = defaultdict(list)
    for idx, precinct in precincts.iterrows():
        highest_intersection_area = 0
        highest_intersection_district = None
        for polygon in district_tree.query(precinct.geometry):
            area = precinct.geometry.intersection(polygon).area/precinct.geometry.area
            if area > highest_intersection_area:
                highest_intersection_area = area
                highest_intersection_district = districts_to_rows[polygon.centroid.wkt]['Code']
        if highest_intersection_district is None:
            sys.stderr.write("Could not assign a district to precinct with ID: %s\n" % precinct['GEOID10'])
        else:
            district_to_precincts[highest_intersection_district].append(precinct['GEOID10'])
    for d, p in district_to_precincts.items():
        print("Found %d precincts for district %s" % (len(p), d))

    return district_to_precincts

def get_district_election_data(ids, elections):
    district_elections = defaultdict(int)
    for prid in ids:
        if prid in elections:
            for party in elections[prid]:
                district_elections[party] += elections[prid][party]
        else:
            suffix_char = 'A'
            while (prid + suffix_char) in elections:
                for party in elections[prid + suffix_char]:
                    district_elections[party] += elections[prid + suffix_char][party]
                suffix_char = chr(ord(suffix_char) + 1)

    return district_elections

def get_district_demographic_data(ids, demographics):
    district_demographics = defaultdict(int)
    for prid in ids:
        if prid in demographics:
            for ethnicity in demographics[prid]:
                district_demographics[ethnicity] += demographics[prid][ethnicity]

    return district_demographics

def get_district_neighbors(district, all_districts, strtree, districts_to_rows):
    neighbors = []
    for polygon in strtree.query(district.geometry):
        if district.geometry.touches(polygon):
            neighbors.append(districts_to_rows[polygon.centroid.wkt]['Code'])

    return neighbors


def get_districts_geojson(state_code):
    district_count = DISTRICT_COUNTS[state_code]
    all_districts = []
    for i in range(1, district_count + 1):
        geojson_file_url = "https://theunitedstates.io/districts/cds/2016/%s-%d/shape.geojson" % (state_code, i)
        district_df = geopandas.read_file(geojson_file_url)
        copy_df = district_df.copy()
        copy_df = copy_df.to_crs({'init': 'epsg:3857'})
        district_df['area'] = copy_df['geometry'].area
        all_districts.append(district_df)
    return geopandas.GeoDataFrame(pandas.concat(all_districts))

if __name__ == '__main__':
    main()
