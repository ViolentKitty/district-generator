const TEXAS_ZOOM_COORDS = [30.110981559253847, -100.61279296875001];
const TEXAS_ZOOM_LEVEL = 6;
const MISSOURI_ZOOM_COORDS = [37.563306750622885, -93.416748046875];
const MISSOURI_ZOOM_LEVEL = 7;
const HAWAII_ZOOM_COORDS = [19.02184128646457, -158.57666015625003];
const HAWAII_ZOOM_LEVEL = 7;
const DEFAULT_ZOOM_COORDS = [26, -137];
const DEFAULT_ZOOM_LEVEL = 4;


// Global initialization -- This can be moved to be loaded from the DB
const texas = {
    name: "Texas",
    abbr: "TX",
    stateBoundaryJSONidx: 12, 
    defaultDistrictCount: 36,
    stateBoundaryLayer: new L.LayerGroup(),
    defaultDistrictLayer: new L.LayerGroup(),
    precinctsLayer:  new L.LayerGroup(), // L.geoJSON(tx_data).setStyle({weight: 1 }),
    colors: [],
}

const missouri = {
    name: "Missouri",
    abbr: "MO",
    stateBoundaryJSONidx: 39,
    defaultDistrictCount: 8,
    stateBoundaryLayer: new L.LayerGroup(),
    defaultDistrictLayer: new L.LayerGroup(),
    precinctsLayer:  new L.LayerGroup(), //L.geoJSON(mo_data).setStyle({weight: 1 }),
    colors: [],
}

const hawaii = {
    name: "Hawaii",
    abbr: "HI",
    stateBoundaryJSONidx: 29,
    defaultDistrictCount: 2,
    stateBoundaryLayer: new L.LayerGroup(),
    defaultDistrictLayer: new L.LayerGroup(),
    precinctsLayer:  new L.LayerGroup(), //L.geoJSON(hi_data).setStyle({weight: 1 }),
    colors: [],
}


const states = [texas, missouri, hawaii];

const NO_STATE_IDX = -1;
const TEXAS_IDX = 0;
const MISSOURI_IDX = 1;
const HAWAII_IDX = 2;

const map = L.map('mapid', { zoomControl: false }).setView(DEFAULT_ZOOM_COORDS, DEFAULT_ZOOM_LEVEL);
// Added this because the left window was blocking the zoom button upon extending it
L.control.zoom({ position: 'topright' }).addTo(map);

const CartoDB_Positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
	subdomains: 'abcd',
	maxZoom: 19
}).addTo(map);

