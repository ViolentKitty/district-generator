
let stateBoundaryToggle = false;
let defaultDistrictBoundaryToggle = false;
let selectedState = 2;
let incrementBatchMeasures = {};
let allBatchResults = [];

$(document).ready(function () {

    // initialize general UI handlers
    initializeUIHandlers();

    $("#texasButton").on("click", () => showPrecinctDataForState(TEXAS_IDX));
    $("#missouriButton").on("click", () => showPrecinctDataForState(MISSOURI_IDX));
    $("#hawaiiButton").on("click", () => showPrecinctDataForState(HAWAII_IDX));


    $("#stateBoundaryToggle").on("click", function(event) {
        states.forEach((state) => {
            state.stateBoundaryLayer.getLayers().forEach((layer) => {
                if(!stateBoundaryToggle){
                    map.addLayer(layer);
                }
                else{
                    map.removeLayer(layer);
                }
            });
        });
        stateBoundaryToggle =! stateBoundaryToggle;
    });
    $("#defaultDistrictBoundaryToggle").on("click", function(event) {
        states.forEach((state) => {
            state.defaultDistrictLayer.getLayers().forEach((layer, idx) => {
                layer.setStyle({fillColor: state.colors[idx]});
                if(!defaultDistrictBoundaryToggle){
                    map.addLayer(layer);
                }
                else{
                    map.removeLayer(layer);
                }
            });
        });
        defaultDistrictBoundaryToggle = ! defaultDistrictBoundaryToggle;
    });

    

    addStateLayers();
    addDistrictLayers(TEXAS_IDX);
    addDistrictLayers(MISSOURI_IDX);
    addDistrictLayers(HAWAII_IDX);

});


function initializeUIHandlers() {

    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss').on('click', function () {
        // hide sidebar
        $('#sidebar').toggleClass('active');
    });

    $('#calculate').on('click', function () {
        // hide sidebar
        $('#sidebar').toggleClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        // open or close navbar
        $('#sidebar').toggleClass('active');
        // close dropdowns
        $('.collapse.in').toggleClass('in');
        // and also adjust aria-expanded attributes we use for the open/closed arrows
        // in our CSS
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    $('#signUpBtn').on('click', function () {
        location.href = "register.html";
    });

    $('.input').focus(function () {
        $(this).parent().find(".label-txt").addClass('label-active');
    });

    $(".input").focusout(function () {
        if ($(this).val() == '') {
            $(this).parent().find(".label-txt").removeClass('label-active');
        };
    });

    $('.dropdown-menu a').click(function(){
        $('#stateDropDownMenu').text($(this).text());
      });

    

    $("#wrapper").toggleClass("toggled");

    $("#savemap").click((e) => {
        e.preventDefault();
        url = "http://localhost:8080/savedmaps/save";
        fetch(url).then((r) => {
            if (r.ok) {
                alert("Saved map");
            } else {
                console.log("Something went wrong saving map");
            }
        });
    });

    $("#savelog").click((e) => {
        e.preventDefault();
        url = "http://localhost:8080/algorithm/savelog";
        fetch(url).then((r) => {
            if (r.ok) {
                alert("Saved log");
            } else {
                console.log("Something went wrong saving log");
            }
        });
    });

    $("#batchRun").click((e) => {
        e.preventDefault();
        $("#algorithmForm").submit();
    });

    $("#algorithmForm").submit((e) => {
        e.preventDefault();
        url = "http://localhost:8080/algorithm/preferences";
        let formData = new FormData();
        formData.append('numDistricts', $("#desiredDistricts").val());
        formData.append('stateName', states[selectedState].name);
        formData.append('isBatch', $("#isBatchRun").checked)
        formData.append('numRuns', $("#batchsize").val());
        formData.append('batchIncrements', states[selectedState].incrementBatchMeasures)
        formData.append('updateStatus', $("#runPhaseOneonly").is(":checked"));
        formData.append('reockMeasure', $("#reock").val() );
        formData.append('schwartzburgMeasure', $("#schwartzburg").val() );
        formData.append('polsbyMeasure',$("#polsbypopper").val() );
        formData.append('convexMeasure',$("#convex").val());
        formData.append('efficiencyGapMeasure',$("#efficiencyGap").val() );
        formData.append('meanMeadianMeasure',$("#meanmedian").val() );
        formData.append('equalPopulation ', $("#equalPopulation").is(":checked") );
        




        fetch(url, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                headers: { "Content-Type": "application/json", },
                mode: "no-cors",
                redirect: "follow", // manual, *follow, error
                body: formData,
        }).then((r) => {
            if (r.ok) {
                console.log("is Batch Run: "+$("#isBatchRun").is(":checked"));
                if(!$("#isBatchRun").is(":checked")){  // Ishan:  If it is not a batch run then do the regular procedure
                    url = "http://localhost:8080/algorithm/partition"
                    good = true;
                    makeRequest = true;
                    clusters = {};
                    states[selectedState].stateResponse['districts'].forEach((d) => {
                        d['precincts'].forEach((p) => {
                            clusters[p['id']] = [p['id']];
                        });
                    });
                    precincts = Object.keys(clusters);
                    numPrecincts = precincts.length;
                    colors = generateRGBArray(numPrecincts);
                    precincts.forEach((p, i) => {
                        states[selectedState].precinctsToLayers[p].setStyle({ fillColor: colors[i], weight: 0.1 });
                    });
                    batchPartition = !$("#runPhaseOneonly").is(":checked");
                    interval = setInterval(() => {
                        if (batchPartition){
                            clearInterval(interval);
                        }
                        fetch(url).then((partitionResponse) => {
                            if (partitionResponse.ok) {
                                return partitionResponse.json();
                            } else if (!partitionResponse.ok && !batchPartition) {
                                clearInterval(interval);
                                $("#redistrict").attr('disabled', 'false');
                                $("#redistrict").removeAttr('disabled');
                                $("#calculate").attr('disabled', 'disabled');

                                $("#savemap").attr('disabled', 'false');
                                $("#savemap").removeAttr('disabled');
                                
                                new_colors = generateRGBArray($("#desiredDistricts").val());
                                Object.keys(clusters).forEach((c, i) => {
                                    clusters[c].forEach((p) => {
                                        states[selectedState].precinctsToLayers[p].setStyle({ fillColor: new_colors[i], weight: 0.1 });
                                        return null;
                                    });
                                });
                            } else {
                                clearInterval(interval);
                                console.log("Batch partition failed");
                                return null;
                            }
                        }).then((partitionResponses) => {
                            if (partitionResponses == null)
                                return;
                            partitionResponses.forEach((partitionResponse) => {
                                source = partitionResponse['source'];
                                target = partitionResponse['target'];
                                srcIdx = precincts.indexOf(source.toString());
                                clusters[target].forEach((p) => {
                                    clusters[source].push(p);
                                });
                                clusters[source].forEach((p) => {
                                    states[selectedState].precinctsToLayers[p].setStyle({ fillColor: colors[srcIdx], weight: 0.1 });
                                });
                                delete clusters[target];
                            });
                            if (batchPartition){
                                $("#redistrict").attr('disabled', 'false');
                                $("#redistrict").removeAttr('disabled');
                                $("#calculate").attr('disabled', 'disabled');
                                $("#savemap").attr('disabled', 'false');
                                $("#savemap").removeAttr('disabled');
                            }
                        });
                    }, 50);
                }
                else{
                    batchRun(0); // This is the batch run call
                }
            } 
        });
    });


    function batchRun(runNum){
        url = "http://localhost:8080/algorithm/singlebatchrun?"
        runNumString = "runNum="
        requestedBatchRunsString = "&requestedRunNum="+$("#batchsize").val();
        if(runNum >= $("#batchsize").val()){
            
            form = document.getElementById("algorithmForm");
            div = document.createElement("div");
            allBatchResults.forEach((result, i ) => {
                child = document.createElement("div");

                button = document.createElement("button");
                button.innerHTML = "Evolution " + (i + 1);
                button.class = "btn btn-info btn-med";
                button.name = "" + i;
                button.addEventListener("click", e => processBatchResult(allBatchResults[parseInt(button.name)]));
                child.appendChild(document.createTextNode("Partition Count: " + result['partitionMoveCount'] + ", Redistrict Count: " + result['redistrictMoveCount']))

                child.appendChild(button);
                div.appendChild(child);
                div.appendChild(document.createElement("br"));
            });
            form.appendChild(div);
            return null;
        }
        good = false;

        prefurl = "http://localhost:8080/algorithm/preferences";
        let formData = new FormData();
        formData.append('numDistricts', $("#desiredDistricts").val());
        formData.append('stateName', states[selectedState].name);
        formData.append('isBatch', $("#isBatchRun").checked)
        formData.append('numRuns', $("#batchsize").val());
        formData.append('batchIncrements', states[selectedState].incrementBatchMeasures)
        formData.append('updateStatus', $("#runPhaseOneonly").is(":checked"));
        formData.append('reockMeasure', $("#reock").val() );
        formData.append('schwartzburgMeasure', $("#schwartzburg").val() );
        formData.append('polsbyMeasure',$("#polsbypopper").val() );
        formData.append('convexMeasure',$("#convex").val());
        formData.append('efficiencyGapMeasure',$("#efficiencyGap").val() );
        formData.append('meanMeadianMeasure',$("#meanmedian").val() );
        formData.append('equalPopulation ', $("#equalPopulation").is(":checked") );
        


        fetch(prefurl, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                headers: { "Content-Type": "application/json", },
                mode: "no-cors",
                redirect: "follow", // manual, *follow, error
                body: formData,
        }).then(r => r.ok).then((r) => {
            if (r) {
                fetch(url+runNumString+runNum+requestedBatchRunsString, {
                    method: "GET", // *GET, POST, PUT, DELETE, etc.
                }).then((r) => {
                    console.log(r);
                    if(r.ok){
                        return r.json();
                    }
                    else{
                        return null;
                    }
                }).then((batchResult) => {
                    if(batchResult == null){
                        return null;
                    }
                    console.log(batchResult);
                    allBatchResults.push(batchResult);
        
                    // at this point r is json obj
                    // Map the state onto the leaflet
                    useEvolutionPlayer = $("#evolutionPlayer").is(":checked");
                    if(useEvolutionPlayer){
                        processBatchResult(batchResult);
                    }
                    else{
                        
                    }
                    batchRun(runNum + 1); // recurse
                });
            } else {
                console.log("Couldnt set preferences on batch run");
            }
        })



        
    }

    function processBatchResult(batchResult){

        partitionMoves = batchResult['partitionMoves'];
        redistrictMoves = batchResult['redistrictMoves'];
        console.log("Starting Evolution Player for partitioning");
        clusters = {};
        states[selectedState].stateResponse['districts'].forEach((d) => {
            d['precincts'].forEach((p) => {
                clusters[p['id']] = [p['id']];
            });
        });
        precincts = Object.keys(clusters);
            numPrecincts = precincts.length;
            colors = generateRGBArray(numPrecincts);
            precincts.forEach((p, i) => {
                states[selectedState].precinctsToLayers[p].setStyle({ fillColor: colors[i], weight: 0.1 });
            }); // This basically
        partitionMoves.forEach((partitionResponse) => {
            source = partitionResponse['source'];
            target = partitionResponse['target'];
            srcIdx = precincts.indexOf(source.toString());
            clusters[target].forEach((p) => {
                clusters[source].push(p);
            });
            clusters[source].forEach((p) => {
                states[selectedState].precinctsToLayers[p].setStyle({ fillColor: colors[srcIdx], weight: 0.1 });
            });
            delete clusters[target];
        });
        console.log(clusters);
        console.log("Starting Evolution Player for redistricting");
        redistrictMoves.forEach((r) => {
            precinct = r['precinct'];
            source = r['source'];
            destination = r['destination'];

            console.log("Moved " + precinct + " from " + source + " to " + destination);

            clusters[source.toString()].splice(clusters[source.toString()].indexOf(precinct.toString()), 1);
            clusters[destination.toString()].push(precinct.toString());

            destIdx = precincts.indexOf(destination.toString());
            states[selectedState].precinctsToLayers[precinct].setStyle({ fillColor: colors[destIdx], weight: 0.1 });

        });
        
    }

    function startRedistrict(){
        url = "http://localhost:8080/algorithm/redistrict";
        fetch(url).then((r) => {
            if (r.ok) {
                return r.json();
            } else {
                console.log("Redistricting done");
                $("#savelog").removeAttr('disabled');
                clearInterval(interval);
                return null;
            }
        }).then((r) => {
            if (r == null){
                return;
            }
            precinct = r['precinct'];
            source = r['source'];
            destination = r['destination'];

            console.log("Moved " + precinct + " from " + source + " to " + destination);

            clusters[source.toString()].splice(clusters[source.toString()].indexOf(precinct.toString()), 1);
            clusters[destination.toString()].push(precinct.toString());

            destIdx = precincts.indexOf(destination.toString());
            states[selectedState].precinctsToLayers[precinct].setStyle({ fillColor: colors[destIdx], weight: 0.1 });

            startRedistrict();

        });
    };

    $("#redistrict").click((e) => {
        e.preventDefault();
        startRedistrict();
    });

    $("#loginform").submit((e) => {
        e.preventDefault();
        url = "http://localhost:8080/login";
        let formData = new FormData();
        formData.append('email', $("#usernameInput").val());
        formData.append('password', $("#passwordInput").val());

        fetch(url, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                headers: { "Content-Type": "application/json", },
                mode: "no-cors",
                redirect: "follow", // manual, *follow, error
                body: formData,
        }).then(response => {
            console.log(response);
            if (response.ok) {
                alert("Login success");

                /*
                // TODO: Update GUI 
                var guestUI = $("#guestUI");
                guestUI.style.display = "none";

                var userUI = $("#userUI");
                userUI.style.display = "block";
                */



                 
            } else { 
                alert("Login fail");
            }
        }).catch(e => alert("Login fail " + e));

    });
}

// parses the district boundary for each state from array of states
function addDistrictLayers(stateIdx) {
    var state = states[stateIdx];
    state.colors = generateRGBArray(state.defaultDistrictCount);
    for (i = 1; i <= state.defaultDistrictCount; i++) {
        var url = "https://theunitedstates.io/districts/cds/2016/" + state.abbr + "-" + i + "/shape.geojson";
        fetch(url).then(data => data.json()).then(data => {
            state.defaultDistrictLayer.addLayer(L.geoJSON(data));
        });
    }
}

function displayBatchInput() {
    var checkBox = document.getElementById("isBatchRun");
    // Get the output text
    var text = document.getElementById("batchParameters");
    console.log(checkBox.checked)
    // If the checkbox is checked, display the output text
    if (checkBox.checked == true){
      text.style.display = "block";
    } else {
      text.style.display = "none";
    }
}

function batchIncrement(id){
    if(!(id in incrementBatchMeasures)){
        incrementBatchMeasures[id] = true;
    }
    else{
        incrementBatchMeasures[id] = !incrementBatchMeasures[id];
    }
}

function generateMMInputFields() {
    var numMMDistricts = document.getElementById("desiredMMDistricts").value;
    var ulofMMDistricts = document.getElementById("listofMMdistricts");
    if (numMMDistricts > 0) {
    ulofMMDistricts.style.display = "block";
    } else {
    ulofMMDistricts.style.display = "none";
    }
    while (ulofMMDistricts.firstChild) {
    ulofMMDistricts.removeChild(ulofMMDistricts.firstChild);
    }
    for (var i = 1; i <= numMMDistricts; i++) {
        var newElement = document.createElement('li');
        newElement.innerHTML = '<div id="MMdistrict' + i + '><b class="MMDistrictTitleNum">Majority-Minority District ' + i + '</b><br>Select Ethic Group<select id="MMethicgroup" class = "MMethicgroup-dropdown"><option value = "black">African American</option><option value = "aian">American Indian and Alaska Native</option><option value = "asian">Asian</option><option value = "hispanic">Hispanic</option><option value = "nhopi">Native Hawaiian and other Pacific Islander</option></select><br><br>Maximum Population %<input type="range" class="range-slider__range" value="0" min="0" max="100" step="1" id="MMmaxPopPercentage"><span class="range-slider__value">0</span><br><br>Minimum Population %<input type="range" class="range-slider__range" value="0" min="0" max="100" step="1" id="MMminPopPercentage"><span class="range-slider__value">0</span><hr></div>';
        ulofMMDistricts.appendChild(newElement);
    }
    rangeSlider();
}

// parses state boundary data
function addStateLayers() {
    var stateBoundaryJSONurl = "https://raw.githubusercontent.com/iks1012/sampledataset/master/50statesdata.json";
    fetch(stateBoundaryJSONurl).then(data => data.json()).then(data => data['features']).then(data => { 
        states.forEach((state) => {
            state.stateBoundaryLayer.addLayer(L.geoJSON(data[state.stateBoundaryJSONidx]));
        });
    });
}

function loadMap(mapid) {
    url = "http://localhost:8080/savedmaps/load?savedMapId=" + mapid;
    fetch(url).then((r) => {
        if (r.ok) { 
            return r.json();
        } else {
            console.log("load map failed");
        }
    }).then((r) => {
        numDistricts = Object.keys(r).length;
        colors = generateRGBArray(numDistricts);
        Object.keys(r).forEach((rkey, i) => {
            r[rkey].forEach((p) => {
                states[selectedState].precinctsToLayers[p].setStyle({ fillColor: colors[i], weight: 0.1 });
            });
        });
        
    });
}

function deleteMap(){
    url = "http://localhost:8080/savedmaps/delete?savedMapId=" + document.getElementById('loadMapList').value;
    fetch(url).then((r) => {
        if (r.ok) { 
            document.getElementById("loadMapList").removeChild(document.getElementById('map' + document.getElementById('loadMapList').value));
        } else {
            console.log("delete map failed");
        }
    });

}

// renders selected state's precinct data to map 
function showPrecinctDataForState(stateIndex){
    if (selectedState != NO_STATE_IDX){
        states[selectedState].precinctsLayer.getLayers().forEach((l) => {
            map.removeLayer(l);
        });
    }
    selectedState = stateIndex;
    if(states[selectedState].precinctsLayer.getLayers().length == 0){
        // populate states[selectedState].precinctsLayer 
        url = "http://localhost:8080/guest/select?selectedState="+selectedState
        document.title = "LOADING";
        fetch(url, {
            method: "GET",
            headers: new Headers(),
            mode: "cors"
        }).then(response => {
            if (response.ok){
                return response.json();
            } else {
                throw Error();
            }
        })
        .then(response => {
            console.log(response);
            states[selectedState].stateResponse = response;
            map.setView([response['latitude'], response['longitude']], response['zoomLevel']);
            fetch("http://localhost:8080/savedmaps/loadall").then((r) => {
                if (r.ok) {
                    return r.json();
                } else {
                    console.log("Could not load maps");
                }
            }).then((r) => {
                Object.keys(r).forEach((rkey) => {
                    option = document.createElement("option");
                    option.innerHTML = rkey;
                    option.value = rkey;
                    option.id = "map" + rkey;
                    document.getElementById("loadMapList").appendChild(option);
                });
            });
            states[selectedState].zoomLevel = response['zoomLevel'];
            states[selectedState].latitude = response['latitude'];
            states[selectedState].longitude = response['longitude'];
            states[selectedState].precinctsToLayers = {}
            response['districts'].forEach((d) => {
                d['precincts'].forEach((p) => {
                    temp_layer = L.geoJSON(JSON.parse(p['geometryAsText'])).setStyle({weight: 1 })
                    states[selectedState].precinctsToLayers[p['id']] = temp_layer;
                    states[selectedState].precinctsLayer.addLayer(temp_layer)


                    const element = document.createElement("div").appendChild(document.createElement("div"));

                    // build tooltip html element -- this will be different for each state
                    var header1 = document.createElement("div");
                    header1.appendChild(document.createTextNode("Precinct-Level Information | ID: "+p['id']));
                    header1.style.fontSize = "large";

                    var header2 = document.createElement("div");
                    header2.appendChild(document.createTextNode("Precinct-Level Demographic Data:"));
                    header2.style.fontSize = "medium";

                    var header3 = document.createElement("div");
                    header3.appendChild(document.createTextNode("Precinct-Level Election Data:"));
                    header3.style.fontSize = "medium";
                    element.appendChild(header1);
                    element.appendChild(document.createElement("hr"));
                    element.appendChild(header2);
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("Total Population: "+ getCommaNumber(p['demographic']['totalPopulation'])));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("Caucasian Population: "+getCommaNumber(p['demographic']['caucasianPopulation'])));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("African American Population: "+getCommaNumber(p['demographic']['africanAmericanPopulation'])));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("Hispanic Population: "+getCommaNumber(p['demographic']['hispanicPopulation'])));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("Asian Population: "+getCommaNumber(p['demographic']['asianPopulation'])));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("American Indian and Alaskan Native Population: "+getCommaNumber(p['demographic']['aianPopulation'])));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("Native Hawaiian and Other Pacific Islander Population: "+getCommaNumber(p['demographic']['pacificIslanderPopulation'])));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("Other Population: "+getCommaNumber(p['demographic']['otherPopulation'])));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createElement("hr"));
                    element.appendChild(header3);
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("Total Votes:"+ getCommaNumber(p['election']['democratCount']+p['election']['republicCount']) ));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("Republican votes: "+getCommaNumber(p['election']['republicCount'] )));
                    element.appendChild(document.createElement("br"));
                    element.appendChild(document.createTextNode("Democrat votes: "+getCommaNumber(p['election']['democratCount'])));
                    element.appendChild(document.createElement("br"));

                    map.addLayer(temp_layer);
                    temp_layer.bindTooltip(element).addTo(map);
                    temp_layer.getTooltip().setOpacity(0.6);
                });
            });
        });
    }
    else{

        map.setView([states[selectedState]['latitude'], states[selectedState]['longitude']], states[selectedState]['zoomLevel']);
        map.addLayer(states[selectedState].precinctsLayer);
    }

    document.title = "ASTROS";
    
}


function getCommaNumber(number){
    string = number.toString()
    var commaCounter = 0;
    var final_number_string = "";
    for(var i = 0; i < string.length; i++){
        final_number_string = string.charAt(string.length - i - 1) + final_number_string; // append to the front;
        if(commaCounter == 2 && (string.length - i - 1) > 0 ){ // second condition is there so we dont get like ",555,555"
            final_number_string = ',' + final_number_string;
        }
        commaCounter = (commaCounter + 1) % 3;
    }
    return final_number_string;
}

var rangeSlider = function(){
    var slider = $('.range-slider'),
        range = $('.range-slider__range'),
        value = $('.range-slider__value');
      
    slider.each(function(){
  
      value.each(function(){
        var value = $(this).prev().attr('value');
        $(this).html(value);
      });
  
      range.on('input', function(){
        $(this).next(value).html(this.value);
      });
    });
  };
  
  rangeSlider();
