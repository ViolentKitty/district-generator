/*
    Generates unique RGB (high contrast) colors through 
    partitioning the hue in HSV as maximizing gap (max possible hStep)between
    hue values gives the most unique colors possible.
*/
function generateRGBArray(n){
    colors = []
    for(i = 0; i < n; i++){
        colors.push(getRandomColor());
    }
    return colors;
//
//    step = 255 / Math.pow(n, (1/3.0));
//    colors = []
//    for (r = 0; r < 255; r+= step){
//        for (g = 0; g < 255; g+= step){
//            for (b = 0; b < 255; b += step){
//                color = 'rgb(' + r + ', ' + g + ', ' + b + ')';
//                colors.push(color);
//            }
//        }
//    }
//    return shuffleArray(colors);
//    var hue = 0;
//    var hStep = 360/n;
//    var hValues = new Array(n);
//    for(i = 0; i < n; i++){
//        hValues[i] = hue;
//        hue += hStep;
//    }
//
//
//    var rgbArray = new Array(n);
//
//
//    for(vCounter = 0; vCounter < 2; vCounter++){
//        var vVal = 100.0/(1.0+vCounter);
//        for(i = 0; i < n/2; i++){
//            rgbArray[i+(vCounter * n/2)] = HSVtoRGB(hValues[i + (vCounter * n/2)], 1, vVal/100.0);
//        }
//    }
//
//    return shuffleArray(rgbArray);
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
// 0 <= H < 360
// 0 <= S <= 1
// 0 <= V <= 1
// this is a formula pls do not dispute
function HSVtoRGB(h,s,v){
    var c = v * s;
    var x = c * (1 - Math.abs((h/60)%2-1));
    var m = v - c;

    var rPrime = -1;
    var gPrime = -1;
    var bPrime = -1;
    hPointer = Math.floor(h/60);
    switch(hPointer){
        case 0:
            rPrime = c;
            gPrime = x;
            bPrime = 0;
        break;

        case 1:
            rPrime = x;
            gPrime = c;
            bPrime = 0;
        break;

        case 2:
            rPrime = 0;
            gPrime = c;
            bPrime = x;
        break;

        case 3:
            rPrime = 0;
            gPrime = x;
            bPrime = c;
        break;

        case 4:
            rPrime = x;
            gPrime = 0;
            bPrime = c;
        break;

        case 5:
            rPrime = c;
            gPrime = 0;
            bPrime = x;
        break;
    }


    var rVal = Math.round((rPrime + m)*255);
    var gVal = Math.round((gPrime + m)*255);
    var bVal = Math.round((bPrime + m)*255);

    return ("#" + rVal.toString(16).padStart(2, "0") + gVal.toString(16).padStart(2, "0") + bVal.toString(16).padStart(2, "0"));
}



// Shuffles an array of objects -- need this for the coloring of districts (As per use case list)
function shuffleArray(arr){
    var swapIndex = -1;
    for(i = arr.length; i > 0; i--){
        swapIndex = Math.floor(Math.random() * (i));
        var temp = arr[i-1];
        arr[i-1] = arr[swapIndex];
        arr[swapIndex] = temp;
    }
    return arr;
}

