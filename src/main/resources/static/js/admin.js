$(document).ready(function() {
    $('#getUsers').click(function() {
        loadUsers();
    });

    function loadUsers() {
        $("#usersTable").jsGrid({
            height: "auto",
            width: "100%",
            deleting: true,
            sorting: true,
            paging: true,
            confirmEditing: false,
            confirmDeleting: false,
            autoload: true,
            controller: {
                loadData: function () {
                    var d1 = $.Deferred();
                    $.ajax({
                        type: "get",
                        dataType: "JSON",
                        url: "/admin/users/all"
                    }).done(function(response) {
                        d1.resolve(response);
                    });
                    return d1.promise();
                }
            },
            fields: [
                { name: "id", title: "Id", type: "number", width: 150 },
                { name: "email", title: "Email", type: "text", width: 150 },
                //{ name: "password", title: "Password", type: "text", width: 150 },
                { type: "control", width: 100, editButton: false, deleteButton: false,
                    itemTemplate: function(value, item) {
                        var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

                        var $customEditButton = $("<button>").attr({class: "customGridEditbutton jsgrid-button jsgrid-edit-button"})
                            .click(function(e) {
                                $.ajax({
                                    type: "get",
                                    dataType: "JSON",
                                    url: "/admin/users/makeadmin?id=" + item.id + ""
                                }).done(function(response) {
                                    $("#usersTable").jsGrid("deleteItem", item);
                                });
                            });

                        var $customDeleteButton = $("<button>").attr({class: "customGridDeletebutton jsgrid-button jsgrid-delete-button"})
                            .click(function(e) {
                                $.ajax({
                                    type: "get",
                                    dataType: "JSON",
                                    url: "/admin/users/delete?id=" + item.id + ""
                                }).done(function(response) {
                                    $("#usersTable").jsGrid("deleteItem", item);
                                });
                            });

                        return $("<div>").append($customEditButton).append($customDeleteButton);
                        //return $result.add($customButton);
                    },
                }
            ]
        });
    }

});
