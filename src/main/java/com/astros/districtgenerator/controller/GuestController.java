package com.astros.districtgenerator.controller;

import com.astros.districtgenerator.algorithm.Algorithm;
import com.astros.districtgenerator.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class GuestController {

    @Autowired
    private Algorithm algorithm;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/guest/select")
    public ResponseEntity<?> selectState(int selectedState) {
        logger.error("selected");
        algorithm.initializeState(selectedState);
        return ResponseEntity.status(HttpStatus.OK).body(algorithm.getState());
    }

    @GetMapping("/guest/district/demographic")
    public ResponseEntity<?> getDistrictDemographic(int districtId) {
        State state = algorithm.getState();
        if (state == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
        Demographic demographic = state.getDistrictById(districtId).getDemographic();
        if (demographic == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
        return ResponseEntity.status(HttpStatus.OK).body(Collections.singletonMap("data", demographic));
    }

    @GetMapping("/guest/precinct/demographic")
    public ResponseEntity<?> getPrecinctDemographic(long precinctId) {
        State state = algorithm.getState();
        if (state == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
        Demographic demographic = state.getPrecinctById(precinctId).getDemographic();
        if (demographic == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
        return ResponseEntity.status(HttpStatus.OK).body(Collections.singletonMap("data", demographic));
    }

    @GetMapping("/guest/precinct/election")
    public ResponseEntity<?> getPrecinctElection(long precinctId) {
        State state = algorithm.getState();
        if (state == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
        Election election = state.getPrecinctById(precinctId).getElection();
        if (election == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
        return ResponseEntity.status(HttpStatus.OK).body(Collections.singletonMap("data", election));
    }

}