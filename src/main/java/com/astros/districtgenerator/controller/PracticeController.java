package com.astros.districtgenerator.controller;

import com.astros.districtgenerator.repository.PracticeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;
import java.lang.invoke.MethodType;

@RestController
public class PracticeController {

    @Autowired
    private PracticeRepository practiceRepository;

    public ResponseEntity<?> getAllDemographics() {
        return null;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/practice/all")
    public ResponseEntity<?> getAllPractices() {
        return ResponseEntity.ok(practiceRepository.findAll());
    }

}
