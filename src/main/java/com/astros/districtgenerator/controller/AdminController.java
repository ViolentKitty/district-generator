package com.astros.districtgenerator.controller;

import com.astros.districtgenerator.model.auth.User;
import com.astros.districtgenerator.repository.UserRepository;
import com.astros.districtgenerator.repository.UserRoleRepository;
import com.astros.districtgenerator.service.AdminService;
import com.astros.districtgenerator.service.UserService;
import com.astros.districtgenerator.service.auth.CustomUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class AdminController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AdminService adminService;
    @Autowired
    private UserRoleRepository userRoleRepository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/admin")
    public String getAdminPage() {
        return "admin.html";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/admin/users/all") 
    public ResponseEntity<?> getAllUsers() {
        logger.info("/admin/users/all");
        List<User> generalUsers =
                userRepository.findAll().stream()
                .filter(user -> user.getRoles().size() == 0)
                .collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(generalUsers);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/admin/users/save")
    public ResponseEntity<?> saveUser(User user) {
        logger.info("line check");

        if (user == null) {
            logger.error("user param is null");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }

        logger.info("line check");

        if (adminService.saveUser(user)) {
            logger.info("successfully saved user");
            return ResponseEntity.status(HttpStatus.OK).body(Collections.singletonMap("message", "successfully added user"));
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/admin/users/delete")
    public ResponseEntity<?> deleteUser(long id) {
        if (adminService.deleteUser(id)) {
            return ResponseEntity.status(HttpStatus.OK).body(Collections.singletonMap("message", "successfully deleted user"));
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/admin/users/makeadmin")
    public ResponseEntity<?> makeUserAdmin(long id) {
        userRoleRepository.saveUserRole(id);
        return ResponseEntity.status(HttpStatus.OK).body(Collections.emptyMap());
    }

}
