package com.astros.districtgenerator.controller;

import com.astros.districtgenerator.algorithm.Algorithm;
import com.astros.districtgenerator.model.SavedMap;
import com.astros.districtgenerator.repository.SavedMapRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class SavedMapController {

    @Autowired
    private Algorithm algorithm;
    @Autowired
    private SavedMapRepository savedMapRepository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    // todo: not implemented yet
    @GetMapping("savedmaps/save")
    public ResponseEntity<?> saveMap() {
        boolean success = algorithm.saveCurrentState();

        if (!success) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(Collections.singletonMap("message", "could not save current state"));
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(Collections.singletonMap("message", "current state successfully saved"));
    }

    @GetMapping("/savedmaps/loadall")
    public ResponseEntity<?> loadAllSavedMaps() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        List<SavedMap> savedMaps = savedMapRepository.findAllByEmail(email);

        Map<Long, Map<Long, List<Long>>> idToMap = new HashMap<>();

        List<Map<Long, List<Long>>> listOfd2pl = new ArrayList<>();
        for (SavedMap savedMap : savedMaps) {
            idToMap.put(savedMap.getId(), d2pTod2pl(savedMap.getPrecinctToDistrict()));
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(idToMap);
    }

    @GetMapping("/savedmaps/load")
    public ResponseEntity<?> loadSavedMap(@RequestParam("savedMapId") long savedMapId) {
        Optional<SavedMap> savedMapOptional = savedMapRepository.findById(savedMapId);

        boolean savedMapExists = savedMapOptional.isPresent();
        if (!savedMapExists) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(Collections.emptyMap());
        }

        return ResponseEntity
                .status(HttpStatus.OK)
//                .body(savedMapOptional.get().getPrecinctToDistrict());
                .body(d2pTod2pl(savedMapOptional.get().getPrecinctToDistrict()));
    }

    @GetMapping("/savedmaps/delete")
    public ResponseEntity<?> deleteSavedMap(@RequestParam("savedMapId") long savedMapId) {
        boolean savedMapExists = savedMapRepository.existsById(savedMapId);

        if (!savedMapExists) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Collections.singletonMap("message", "savedMap with specified ID does not exist"));
        }

        savedMapRepository.deleteById(savedMapId);
        return ResponseEntity.status(HttpStatus.OK)
                .body(Collections.singletonMap("message", "successfully deleted savedMap"));
    }

    public Map<Long, List<Long>> d2pTod2pl(Map<Long, Long> d2p) {
        Map<Long, List<Long>> d2pl = new HashMap<>();
        for (Map.Entry<Long, Long> entry : d2p.entrySet()) {
            if (d2pl.containsKey(entry.getValue())) {
                d2pl.get(entry.getValue()).add(entry.getKey());
            } else {
                List<Long> pl = new ArrayList<>();
                pl.add(entry.getKey());
                d2pl.put(entry.getValue(), pl);
            }
        }
        return d2pl;
    }

}
