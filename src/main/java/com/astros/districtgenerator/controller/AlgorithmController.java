package com.astros.districtgenerator.controller;

import com.astros.districtgenerator.algorithm.*;
import com.astros.districtgenerator.model.Cluster;
import com.astros.districtgenerator.model.State;
import com.astros.districtgenerator.repository.StateRepository;
import org.apache.coyote.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@RestController
public class AlgorithmController {

    @Autowired
    private Algorithm algorithm;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/algorithm")
    public void getAlgorithmPage(HttpServletResponse response) throws IOException {
        response.sendRedirect("/");
    }

    @GetMapping("/algorithm/savelog")
    public ResponseEntity<?> saveLogFile() {
        try {
            FileWriter fileWriter = new FileWriter("logfile.txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println("Total partition moves: " + algorithm.getPartitionMoveCount());
            for (List<Cluster> clusterPair : algorithm.getPartitionMoves()) {
                printWriter.println("Cluster (id: " + clusterPair.get(1).getId() + ") was merged into Cluster (id: " + clusterPair.get(0).getId() + ")");
            }
            printWriter.println();
            printWriter.println("Total redistrict moves: " + algorithm.getRedistrictMoveCount());
            for (Move move : algorithm.getRedistrictMoves()) {
                printWriter.println("Precinct (id: " + move.getPrecinctToMove().getId() + ") was moved into District (id: " + move.getTargetDistrict().getId() + ") from District (id: " + move.getSourceDistrict().getId());
            }
            printWriter.close();
            return ResponseEntity.status(HttpStatus.OK).body(Collections.emptyMap());
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
    }

    @PostMapping("/algorithm/preferences")
    public ResponseEntity<?> setUserPreferences(String stateName, int numDistricts, int numRuns, Map<String, Boolean> batchIncrements, boolean updateStatus, float reockMeasure, float schwartzburgMeasure, float polsbyMeasure, float convexMeasure, float efficiencyGapMeasure, float meanMeadianMeasure, Boolean equalPopulation){

        if(!batchIncrements.containsKey("incReock"))
            batchIncrements.put("incReock", false);
        if(!batchIncrements.containsKey("incSchwartzberg"))
            batchIncrements.put("incSchwartzberg", false);
        if(!batchIncrements.containsKey("incPolsby"))
            batchIncrements.put("incPolsby", false);
        if(!batchIncrements.containsKey("incConvex"))
            batchIncrements.put("incConvex", false);
        if(!batchIncrements.containsKey("incEG"))
            batchIncrements.put("incEG", false);
        if(!batchIncrements.containsKey("incMeanMedian"))
            batchIncrements.put("incMeanMedian", false);

        double avg_weight = (reockMeasure + schwartzburgMeasure + polsbyMeasure + convexMeasure + efficiencyGapMeasure + meanMeadianMeasure ) / MeasureType.REOCK_COMPACTNESS.getDeclaringClass().getEnumConstants().length;

        List<Measure> measures = new LinkedList<>();
        measures.add(new Measure(MeasureType.REOCK_COMPACTNESS, reockMeasure, batchIncrements.get("incReock")));
        measures.add(new Measure(MeasureType.SCHWARZBERG_COMPACTNESS, schwartzburgMeasure, batchIncrements.get("incSchwartzberg")));
        measures.add(new Measure(MeasureType.POLSBY_POPPER_COMPACTNESS, polsbyMeasure, batchIncrements.get("incPolsby")));
        measures.add(new Measure(MeasureType.CONVEX_HULL_COMPACTNESS, convexMeasure, batchIncrements.get("incConvex")));
        measures.add(new Measure(MeasureType.EFFICIENCY_GAP, efficiencyGapMeasure, batchIncrements.get("incEG")));
        measures.add(new Measure(MeasureType.MEAN_MEDIAN, meanMeadianMeasure, batchIncrements.get("incMeanMedian")));
        measures.add(new Measure(MeasureType.EQUAL_POPULATION, (equalPopulation) ? 1.0 : 0.0 , false));


        algorithm.setUserPreferences(new UserPreferences(
                stateName,
                numDistricts,
                numRuns,
                updateStatus,
                measures
        ));
        return ResponseEntity.ok(Collections.emptyMap());
    }


    @PostMapping("/algorithm/partition/toggleupdate")
    public ResponseEntity<?> toggleUpdateStatus() {
        algorithm.getUserPreferences().toggleUpdateStatus();
        return ResponseEntity.status(HttpStatus.OK).body(Collections.emptyMap());
    }

    @GetMapping("/algorithm/partition")
    public ResponseEntity<?> partition() {
        if (!(algorithm.getUserPreferences().getUpdateStatus())) {
            List<Map<String, Long>> responses = new ArrayList<>();
            while (true) {
                List<Cluster> clusterPair = algorithm.partition();
                if (clusterPair == null) {
                    break;
                } else {
                    Map<String,Long> response = new HashMap<>(2);
                    response.put("source", clusterPair.get(0).getId());
                    response.put("target", clusterPair.get(1).getId());
                    responses.add(response);
                }
            }
            for (Cluster cluster : algorithm.getClusters()) {
                logger.info(cluster.getId() + " -> " + cluster.getDemographic().getTotalPopulation());
            }
            return ResponseEntity.status(HttpStatus.OK).body(responses);
        }

        List<Cluster> clusterPair = algorithm.partition();
        if (clusterPair == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }

        List<Map<String, Long>> responses = new ArrayList<>();
        Map<String,Long> response = new HashMap<>(2);
        response.put("source", clusterPair.get(0).getId());
        response.put("target", clusterPair.get(1).getId());
        responses.add(response);
        return ResponseEntity.status(HttpStatus.OK).body(responses);
    }

    @GetMapping("/algorithm/redistrict")
    public ResponseEntity<?> getNextMove() {
        Move move = algorithm.redistrict();
        if (move == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }

        Map<String,Long> response = new HashMap<>(3);
        response.put("precinct", move.getPrecinctToMove().getId());
        response.put("source", move.getSourceDistrict().getId());
        response.put("destination", move.getTargetDistrict().getId());
        logger.info("Moving " + move.getPrecinctToMove().getId() + " From " + move.getSourceDistrict().getId() + " To " + move.getTargetDistrict().getId());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping("/algorithm/batchrun") // +"?requestedRunNum="
    public ResponseEntity<?> runBatch(int requestedRunNum) {

        if (requestedRunNum <= 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
        List<BatchResult> batchResults = new ArrayList<>();
        int runNum = 1;
        while (runNum <= requestedRunNum) {
            batchResults.add(algorithm.runSingleBatch(runNum, requestedRunNum));
            runNum ++;
        }
        return ResponseEntity.status(HttpStatus.OK).body(batchResults);
    }


    @GetMapping("/algorithm/singlebatchrun") // +"?runNum=%d&requestedRunNum=%d
    public ResponseEntity<?> runSungleBatchWrapper(int runNum, int requestedRunNum) {
        if (requestedRunNum <= 0 || runNum > requestedRunNum || runNum < 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.emptyMap());
        }
        logger.info("Doing Batch Run");
        BatchResult batchResult = algorithm.runSingleBatch(runNum, requestedRunNum);
        return ResponseEntity.status(HttpStatus.OK).body(batchResult);
    }
}
