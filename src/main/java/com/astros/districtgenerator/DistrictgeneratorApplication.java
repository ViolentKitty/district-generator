package com.astros.districtgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistrictgeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistrictgeneratorApplication.class, args);
	}

}
