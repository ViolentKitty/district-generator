package com.astros.districtgenerator.repository;

import com.astros.districtgenerator.model.PrecinctNeighbor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrecinctNeighborRepository extends JpaRepository<PrecinctNeighbor, Integer> {
}
