package com.astros.districtgenerator.repository;

import com.astros.districtgenerator.model.SavedMap;
import com.astros.districtgenerator.model.auth.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SavedMapRepository extends JpaRepository<SavedMap, Long> {

    List<SavedMap> findAllByEmail(String email);

}
