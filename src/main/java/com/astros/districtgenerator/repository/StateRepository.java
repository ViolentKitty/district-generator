package com.astros.districtgenerator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.astros.districtgenerator.model.State;
import org.springframework.stereotype.Repository;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {
}
