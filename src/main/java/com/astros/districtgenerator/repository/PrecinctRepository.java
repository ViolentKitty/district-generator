package com.astros.districtgenerator.repository;

import com.astros.districtgenerator.model.Precinct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrecinctRepository extends JpaRepository<Precinct, Integer> {
    List<Precinct> findAllByStateId(int stateid);
}
