package com.astros.districtgenerator.repository;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class UserRoleRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void saveUserRole(long userId) {
        entityManager.createNativeQuery("INSERT INTO user_role (user_id) VALUES (?)")
                .setParameter(1, userId)
                .executeUpdate();
    }

}
