package com.astros.districtgenerator.repository;

import com.astros.districtgenerator.model.Demographic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DemographicRepository extends JpaRepository<Demographic, Integer> {
}
