package com.astros.districtgenerator.repository;

import com.astros.districtgenerator.model.DistrictNeighbor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DistrictNeighborRepository extends JpaRepository<DistrictNeighbor, Integer> {
}
