package com.astros.districtgenerator.service;

import com.astros.districtgenerator.model.auth.User;
import com.astros.districtgenerator.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class AdminService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public boolean saveUser(User user) {
        if (userRepository.findByEmail(user.getEmail()) != null) {
            logger.error("user with this email already exists");
            return false;
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return true;
    }

    public boolean deleteUser(long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if (!(optionalUser.isPresent())) {
            logger.error("user doesnt exist");
            return false;
        }

        User user = optionalUser.get();
        if (user.getRoles().size() > 0) {
            logger.error("specified user is an admin - cannot delete");
            return false;
        }

        userRepository.deleteById(userId);
        return true;
    }

}
