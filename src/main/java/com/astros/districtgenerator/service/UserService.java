package com.astros.districtgenerator.service;

import com.astros.districtgenerator.model.auth.User;
import com.astros.districtgenerator.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public boolean saveUser(User user) {
        if (userRepository.findByEmail(user.getEmail()) != null) {
            return false;
        }

        userRepository.save(user);
        return true;
    }

}
