package com.astros.districtgenerator.service.auth;

import com.astros.districtgenerator.model.auth.User;
import com.astros.districtgenerator.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        User user = userRepository.findByEmail(email);

        if (user == null) {
            logger.error("user with email: " + email + " not found");
            throw new UsernameNotFoundException("user with email: " + email + " not found");
        }

        logger.info("user with email: " + email + " found");
        if (user.getRoles() == null) {
            logger.error("------------------------------------------------------------");
        }

        CustomUserDetails customUserDetails = new CustomUserDetails(user);

        if (customUserDetails == null) {
            logger.info("customerUserDetails is null");
            return null;
        } else {
            logger.info("user roles len = " + customUserDetails.getRoles().size());
            logger.info("customerUserDetails isnt null");
            return customUserDetails;
        }


        //return new CustomUserDetails(user);
    }
}
