package com.astros.districtgenerator.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Demographic")
public class Demographic {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "demographic_id")
    private Long id;
    @Column(name = "caucasian")
    private int caucasianPopulation;
    @Column(name = "african_american")
    private int africanAmericanPopulation;
    @Column(name = "hispanic")
    private int hispanicPopulation;
    @Column(name = "asian")
    private int asianPopulation;
    @Column(name = "aian")
    private int aianPopulation;
    @Column(name = "nhopi")
    private int pacificIslanderPopulation;
    @Column(name = "other")
    private int otherPopulation;
    @Column(name = "total")
    private int totalPopulation;

    public Demographic addDemographic(Demographic demographic) {
        this.setCaucasianPopulation(this.getCaucasianPopulation() + demographic.getCaucasianPopulation());
        this.setAfricanAmericanPopulation(this.getAfricanAmericanPopulation() + demographic.getAfricanAmericanPopulation());
        this.setHispanicPopulation(this.getHispanicPopulation() + demographic.getHispanicPopulation());
        this.setAsianPopulation(this.getAsianPopulation() + demographic.getAsianPopulation());
        this.setAianPopulation(this.getAianPopulation() + demographic.getAianPopulation());
        this.setPacificIslanderPopulation(this.getPacificIslanderPopulation() + demographic.getPacificIslanderPopulation());
        this.setOtherPopulation(this.getOtherPopulation() + demographic.getOtherPopulation());
        this.setTotalPopulation(this.getTotalPopulation() + demographic.getTotalPopulation());
        return this;
    }

    public Demographic subtractDemographic(Demographic demographic) {
        this.setCaucasianPopulation(this.getCaucasianPopulation() - demographic.getCaucasianPopulation());
        this.setAfricanAmericanPopulation(this.getAfricanAmericanPopulation() - demographic.getAfricanAmericanPopulation());
        this.setHispanicPopulation(this.getHispanicPopulation() - demographic.getHispanicPopulation());
        this.setAsianPopulation(this.getAsianPopulation() - demographic.getAsianPopulation());
        this.setAianPopulation(this.getAianPopulation() - demographic.getAianPopulation());
        this.setPacificIslanderPopulation(this.getPacificIslanderPopulation() - demographic.getPacificIslanderPopulation());
        this.setOtherPopulation(this.getOtherPopulation() - demographic.getOtherPopulation());
        this.setTotalPopulation(this.getTotalPopulation() - demographic.getTotalPopulation());
        return this;
    }

    public Demographic() {}

    public Demographic(int caucasianPopulation, int africanAmericanPopulation, int hispanicPopulation, int asianPopulation,
                       int aianPopulation, int pacificIslanderPopulation, int otherPopulation, int totalPopulation) {
        this();
        this.caucasianPopulation = caucasianPopulation;
        this.africanAmericanPopulation = africanAmericanPopulation;
        this.hispanicPopulation = hispanicPopulation;
        this.asianPopulation = asianPopulation;
        this.aianPopulation = aianPopulation;
        this.pacificIslanderPopulation = pacificIslanderPopulation;
        this.otherPopulation = otherPopulation;
        this.totalPopulation = totalPopulation;
    }

    public Demographic(Demographic o) {
        this();
        this.caucasianPopulation = o.caucasianPopulation;
        this.africanAmericanPopulation = o.africanAmericanPopulation;
        this.hispanicPopulation = o.hispanicPopulation;
        this.asianPopulation = o.asianPopulation;
        this.aianPopulation = o.aianPopulation;
        this.pacificIslanderPopulation = o.pacificIslanderPopulation;
        this.otherPopulation = o.otherPopulation;
        this.totalPopulation = o.totalPopulation;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCaucasianPopulation() {
        return caucasianPopulation;
    }

    public void setCaucasianPopulation(int caucasianPopulation) {
        this.caucasianPopulation = caucasianPopulation;
    }

    public int getAfricanAmericanPopulation() {
        return africanAmericanPopulation;
    }

    public void setAfricanAmericanPopulation(int africanAmericanPopulation) {
        this.africanAmericanPopulation = africanAmericanPopulation;
    }

    public int getHispanicPopulation() {
        return hispanicPopulation;
    }

    public void setHispanicPopulation(int hispanicPopulation) {
        this.hispanicPopulation = hispanicPopulation;
    }

    public int getAsianPopulation() {
        return asianPopulation;
    }

    public void setAsianPopulation(int asianPopulation) {
        this.asianPopulation = asianPopulation;
    }

    public int getAianPopulation() {
        return aianPopulation;
    }

    public void setAianPopulation(int aianPopulation) {
        this.aianPopulation = aianPopulation;
    }

    public int getPacificIslanderPopulation() {
        return pacificIslanderPopulation;
    }

    public void setPacificIslanderPopulation(int pacificIslanderPopulation) {
        this.pacificIslanderPopulation = pacificIslanderPopulation;
    }

    public int getOtherPopulation() {
        return otherPopulation;
    }

    public void setOtherPopulation(int otherPopulation) {
        this.otherPopulation = otherPopulation;
    }

    public int getTotalPopulation() {
        return totalPopulation;
    }

    public void setTotalPopulation(int totalPopulation) {
        this.totalPopulation = totalPopulation;
    }

    public String toString() {
        return "" + totalPopulation;
    }

}
