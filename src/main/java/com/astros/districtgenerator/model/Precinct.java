package com.astros.districtgenerator.model;

import com.astros.districtgenerator.model.serializers.SetDistrictConverter;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "Precinct")
public class Precinct {

    @Id // removed generation strategy as they are already predetermined
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "area")
    private double area;

    @Column(name = "perimeter")
    private double perimeter;


    @Column(name = "state_id")
    private int stateId;


    @OneToOne
    @JoinColumn(name = "demographic_id", referencedColumnName = "demographic_id")
    private Demographic demographic;

    @OneToOne
    @JoinColumn(name = "election_id", referencedColumnName = "election_id")
    private Election election;

    @Transient
//    @Column(name = "district_id")
    private int districtId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "district_id")
    private District district;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner")
    private List<PrecinctNeighbor> neighbors;

    @Column(name = "precinct_geometry_wkt", columnDefinition = "MEDIUMTEXT")
    private String geometryAsText;

    @JsonIgnore
    @Transient
    private Geometry geometry;

    @JsonIgnore
    @Transient
    private Cluster cluster;

    private static final Logger LOGGER = LoggerFactory.getLogger(Precinct.class);

    public Precinct() {
        neighbors = new ArrayList<>();
    }


    public Precinct(Long id, String name, double area, double perimeter, Demographic demographic, Election election, District district, List<PrecinctNeighbor> neighbors, String geometryAsText, Cluster cluster) {
        this.id = id;
        this.name = name;
        this.area = area;
        this.perimeter = perimeter;
        this.demographic = demographic;
        this.election = election;
        this.districtId = districtId;
        this.neighbors = neighbors;
        this.geometryAsText = geometryAsText;
        this.cluster = cluster;
    }


    @JsonIgnore
    public boolean isBorderPrecinct() {
        for (PrecinctNeighbor edge : neighbors) {
            Precinct neighbor = edge.getNeighbor();
            if (neighbor.getDistrict().getId() != this.getDistrict().getId()) {
                return true;
            }
        }
        return false;
    }

    public Long getId() {
        return id;
    }

    public double getPerimeter(){
        return perimeter;
    }

    public void setPerimeter(double perimeter){
        this.perimeter = perimeter;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public Demographic getDemographic() {
        return demographic;
    }

    public void setDemographic(Demographic demographic) {
        this.demographic = demographic;
    }

    public Election getElection() {
        return election;
    }

    public void setElection(Election election) {
        this.election = election;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public List<PrecinctNeighbor> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(List<PrecinctNeighbor> neighbors) {
        this.neighbors = neighbors;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Precinct precinct = (Precinct) o;

        return getId() == precinct.getId();
    }

    @Override
    public int hashCode() {
        return Long.hashCode(getId());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getGeometryAsText() {
        return geometryAsText;
    }

    public void setGeometryAsText(String geometryAsText) {
        this.geometryAsText = geometryAsText;
    }

    @Transient
    public Geometry getGeometry() {
        if (this.geometry == null) {
            GeometryJSON geometryJSON = new GeometryJSON();
            StringReader stringReader = new StringReader(this.geometryAsText);
            try {
                this.geometry = geometryJSON.read(stringReader);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this.geometry;
    }

    @Override
    public String toString() {
        return "precinct id: " + this.id;
    }

}