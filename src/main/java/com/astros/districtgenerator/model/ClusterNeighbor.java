package com.astros.districtgenerator.model;


public class ClusterNeighbor {

    private long id;
    private Cluster owner;
    private Cluster neighbor;
    private double countyJoinability;
    private double demographicJoinability;

    public double getTotalJoinability() {
        return this.countyJoinability + this.demographicJoinability;
    }

    public ClusterNeighbor() {}

    public ClusterNeighbor(Cluster owner, Cluster neighbor, double countyJoinability, double demographicJoinability) {
        this();
        this.owner = owner;
        this.neighbor = neighbor;
        this.countyJoinability = countyJoinability;
        this.demographicJoinability = demographicJoinability;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Cluster getOwner() {
        return owner;
    }

    public void setOwner(Cluster owner) {
        this.owner = owner;
    }

    public Cluster getNeighbor() {
        return neighbor;
    }

    public void setNeighbor(Cluster neighbor) {
        this.neighbor = neighbor;
    }

    public double getCountyJoinability() {
        return countyJoinability;
    }

    public void setCountyJoinability(double countyJoinability) {
        this.countyJoinability = countyJoinability;
    }

    public double getDemographicJoinability() {
        return demographicJoinability;
    }

    public void setDemographicJoinability(double demographicJoinability) {
        this.demographicJoinability = demographicJoinability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClusterNeighbor that = (ClusterNeighbor) o;

        if (!getOwner().equals(that.getOwner())) return false;
        return getNeighbor().equals(that.getNeighbor());

    }

    @Override
    public int hashCode() {
        int result = getOwner().hashCode();
        result = 31 * result + getNeighbor().hashCode();
        return result;
    }

}
