package com.astros.districtgenerator.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class Cluster {

    private long id;
    private Set<Precinct> precincts;
    private Demographic demographic;
    private Election election;
    private Set<ClusterNeighbor> neighbors;

    private static final Logger LOGGER = LoggerFactory.getLogger(Cluster.class);

    public Cluster() {
        precincts = new HashSet<>();
        neighbors = new HashSet<>();
    }

    public Cluster(Precinct precinct) {
        this();
        this.id = precinct.getId();
        this.precincts.add(precinct);
        this.demographic = new Demographic(precinct.getDemographic());
        this.election = new Election(precinct.getElection());
    }

    public Cluster(long id, Set<Precinct> precincts, Demographic demographic, Election election,
                   Set<ClusterNeighbor> neighbors) {
        this();
        this.id = id;
        this.precincts = precincts;
        this.demographic = demographic;
        this.election = election;
        this.neighbors = neighbors;
    }

    public void absorbCluster(Cluster cluster) {
        this.precincts.addAll(cluster.getPrecincts());
        this.demographic.addDemographic(cluster.getDemographic());
        this.election.addElection(cluster.getElection());
        this.removeNeighbor(cluster);

        Set<Cluster> commonNeighbors = this.getCommonNeighbors(cluster);
        for (Cluster commonNeighbor : commonNeighbors) {
            this.updateJoinabilityValuesForCommonNeighbor(cluster, commonNeighbor);
            cluster.removeNeighbor(commonNeighbor);
            commonNeighbor.removeNeighbor(cluster);
        }
        cluster.removeNeighbor(this);
        // make any remaining neighbors of cluster neighbors of this
        for (ClusterNeighbor remainingNeighbor : cluster.getNeighbors()) {
            remainingNeighbor.setOwner(this);
            this.neighbors.add(remainingNeighbor);
            LOGGER.info("Size of neighbors of " + remainingNeighbor.getNeighbor().getNeighbors().size());
            LOGGER.info("Remaining Neighbor: " + remainingNeighbor.getNeighbor().getId());
            this.printWithNeighbors(remainingNeighbor.getNeighbor());
            LOGGER.info("Searching for cluster: " + cluster.getId());
            this.printWithNeighbors(cluster);
            remainingNeighbor.getNeighbor().getNeighbors()
                    .stream()
                    .filter(cn -> cn.getNeighbor().equals(cluster))
                    .findFirst()
                    .get()
                    .setNeighbor(this);
        }


    }

    public Set<Cluster> getCommonNeighbors(Cluster cluster) {
        Set<Cluster> selfNeighbors = this.getNeighborsAsClusters();
        Set<Cluster> clusterNeighbors = cluster.getNeighborsAsClusters();
        selfNeighbors.retainAll(clusterNeighbors);
        return selfNeighbors;
    }

    public void updateJoinabilityValuesForCommonNeighbor(Cluster cluster, Cluster commonNeighbor) {
        ClusterNeighbor selfToNeighbor = this.neighbors.stream()
                .filter(clusterNeighbor -> clusterNeighbor.getNeighbor().equals(commonNeighbor))
                .findFirst()
                .get();
        ClusterNeighbor clusterToNeighbor = cluster.getNeighbors().stream()
                .filter(clusterNeighbor -> clusterNeighbor.getNeighbor().equals(commonNeighbor))
                .findFirst()
                .get();

        double newCountyJoinability = (selfToNeighbor.getCountyJoinability() + clusterToNeighbor.getCountyJoinability()) / 2;
        double newDemographicJoinability = (selfToNeighbor.getDemographicJoinability() + clusterToNeighbor.getDemographicJoinability()) / 2;
        selfToNeighbor.setCountyJoinability(newCountyJoinability);
        selfToNeighbor.setDemographicJoinability(newDemographicJoinability);

        ClusterNeighbor commonToSelf = commonNeighbor.getNeighbors().stream()
                .filter(clusterNeighbor -> clusterNeighbor.getNeighbor().equals(this))
                .findFirst()
                .get();
        commonToSelf.setCountyJoinability(newCountyJoinability);
        commonToSelf.setDemographicJoinability(newDemographicJoinability);
    }

    public void printWithNeighbors(Cluster cluster) {
        StringBuilder builder = new StringBuilder();
        builder.append(cluster.getId() + " -> ");
        for (ClusterNeighbor cn : cluster.getNeighbors()) {
            builder.append(cn.getNeighbor().getId() + ", ");
        }
        LOGGER.info(builder.toString());
    }

    public void removeNeighbor(Cluster cluster) {
        Iterator iterator = this.neighbors.iterator();
        while (iterator.hasNext()) {
            ClusterNeighbor neighbor = (ClusterNeighbor) iterator.next();
            if (neighbor.getNeighbor() == cluster) {
                iterator.remove();
                return;
            }
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<Precinct> getPrecincts() {
        return precincts;
    }

    public void setPrecincts(Set<Precinct> precincts) {
        this.precincts = precincts;
    }

    public Demographic getDemographic() {
        return demographic;
    }

    public void setDemographic(Demographic demographic) {
        this.demographic = demographic;
    }

    public Election getElection() {
        return election;
    }

    public void setElection(Election election) {
        this.election = election;
    }

    public Set<ClusterNeighbor> getNeighbors() {
        return neighbors;
    }

    public Set<Cluster> getNeighborsAsClusters() {
        return this.neighbors.stream()
                .map(clusterNeighbor -> clusterNeighbor.getNeighbor())
                .collect(Collectors.toSet());
    }

    public void setNeighbors(Set<ClusterNeighbor> neighbors) {
        this.neighbors = neighbors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cluster cluster = (Cluster) o;

        return getId() == cluster.getId();

    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }
}
