package com.astros.districtgenerator.model;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "election")
public class Election {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "election_id")
    private long id;
    @Column(name = "rep_votes")
    private int republicCount;
    @Column(name = "dem_votes")
    private int democratCount;
    @Column(name = "ind_votes")
    private int independentCount;
    @Column(name = "precinct_id")
    private long precinctId;
    @Transient
    @Column(name = "district_id")
    private int districtId;

    public Election addElection(Election election) {
        this.setRepublicCount(this.getRepublicCount() + election.getRepublicCount());
        this.setDemocratCount(this.getDemocratCount() + election.getDemocratCount());
        this.setIndependentCount(this.getIndependentCount() + election.getIndependentCount());
        return this;
    }

    public Election subtractElection(Election election) {
        this.setRepublicCount(this.getRepublicCount() - election.getRepublicCount());
        this.setDemocratCount(this.getDemocratCount() - election.getDemocratCount());
        this.setIndependentCount(this.getIndependentCount() - election.getIndependentCount());
        return this;
    }

    public Election() {}

    public Election(int republicCount, int democratCount, int independentCount) {
        this.republicCount = republicCount;
        this.democratCount = democratCount;
        this.independentCount = independentCount;
    }

    public Election(Election o){
        this.republicCount = o.republicCount;
        this.democratCount = o.democratCount;
        this.independentCount = o.independentCount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRepublicCount() {
        return republicCount;
    }

    public void setRepublicCount(int republicCount) {
        this.republicCount = republicCount;
    }

    public int getDemocratCount() {
        return democratCount;
    }

    public void setDemocratCount(int democratCount) {
        this.democratCount = democratCount;
    }

    public int getIndependentCount() {
        return independentCount;
    }

    public void setIndependentCount(int independentCount) {
        this.independentCount = independentCount;
    }

    public long getPrecinctId() {
        return precinctId;
    }

    public void setPrecinctId(long precinctId) {
        this.precinctId = precinctId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }
}
