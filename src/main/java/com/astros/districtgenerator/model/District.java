package com.astros.districtgenerator.model;


import com.astros.districtgenerator.algorithm.Algorithm;
import com.astros.districtgenerator.model.serializers.SetDistrictNeighborConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.filter.AreaFunction;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.measure.Measure;
import org.geotools.referencing.CRS;
import javax.measure.quantity.Area;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.locationtech.jts.geom.*;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import si.uom.SI;

import javax.measure.Unit;
import javax.persistence.*;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "district")
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @OneToOne
    @JoinColumn(name = "demographic_id", referencedColumnName = "demographic_id")
    private Demographic demographic;

    @Column(name = "state_id")
    private int stateId;

    @OneToOne
    @JoinColumn(name = "election_id", referencedColumnName = "election_id")
    private Election election;

    @Column(name = "name")
    private String name;

    @Column(name = "area")
    private double area;

    @JsonIgnore
    @Transient
    private double perimeter;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "district")
    private Set<Precinct> precincts;


    @JsonSerialize(converter = SetDistrictNeighborConverter.class)
    @OneToMany(mappedBy = "owner")
    Set<DistrictNeighbor> neighbors;

    @Transient
    private String geometryAsText;

    @JsonIgnore
    @Transient
    private Geometry geometry;


    private static final Logger LOGGER = LoggerFactory.getLogger(District.class);

    public District(Cluster cluster) {
        this();
        this.id = cluster.getId();
        this.demographic = cluster.getDemographic();
        this.election = cluster.getElection();
        for (Precinct precinct : cluster.getPrecincts()) {
            this.precincts.add(precinct);
            precinct.setDistrict(this);
        }
        this.stateId = cluster.getPrecincts().stream().findFirst().get().getStateId();
    }

    public District() {
        name = "";
        area = -1;
        stateId = -1;
        perimeter = -1;
        precincts = new HashSet<>();
        neighbors = new HashSet<>();
    }

    public District(long id, String name, double area, double perimeter, State state, double racialScore,
                    Election election, Demographic demographic, Set<Precinct> precincts, Set<DistrictNeighbor> neighbors) {
        this.id = id;
        this.precincts = precincts;
        this.neighbors = neighbors;
    }

    public void printWithNeighbors(District district) {
        StringBuilder builder = new StringBuilder();
        builder.append(district.getId() + " -> ");
        for (DistrictNeighbor dn : district.getNeighbors()) {
            builder.append(dn.getNeighbor().getId() + ", ");
        }
        LOGGER.info(builder.toString());
    }

    @JsonIgnore
    @Transient
    public List<Precinct> getBorderPrecincts() {
        return this.precincts.stream()
                .filter(Precinct::isBorderPrecinct)
                .collect(Collectors.toList());
    }

    public void addPrecinct(Precinct precinct) {
        this.precincts.add(precinct);
        this.setGeometry(); // recalculate geometry since new precinct is added
        this.setPerimeter();
        this.demographic.addDemographic(precinct.getDemographic());
        this.election.addElection(precinct.getElection());
    }

    public void removePrecinct(Precinct precinct) {
        this.precincts.remove(precinct);
        this.setGeometry();
        this.setPerimeter();
        this.demographic.subtractDemographic(precinct.getDemographic());
        this.election.subtractElection(precinct.getElection());
    }

    @JsonIgnore
    @Transient
    public District getNeighborDistrict(Precinct precinct) {
        for (PrecinctNeighbor edge : precinct.getNeighbors()) {
            Precinct neighbor = edge.getNeighbor();
            if (neighbor.getDistrict() != precinct.getDistrict()) {
                return neighbor.getDistrict();
            }
        }

        LOGGER.error("neighbor not found");
        return null;
    }

    public long getId() {
        return id;
    }

    public District setId(long id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        District district = (District) o;

        return getId() == district.getId();

    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }

    public Set<DistrictNeighbor> getNeighbors() {
        return neighbors;
    }

    public District setNeighbors(Set<DistrictNeighbor> neighbors) {
        this.neighbors = neighbors;
        return this;
    }

    public Set<Precinct> getPrecincts() {
        return precincts;
    }

    public District setPrecincts(Set<Precinct> precincts) {
        this.precincts = precincts;
        return this;
    }

    public Demographic getDemographic() {
        return this.demographic;
    }


    public Election getElection() {
        return this.election;
    }

    public String getGeometryAsText() {
        GeometryJSON geoJson = new GeometryJSON();
        StringWriter writer = new StringWriter();
        try {
            geoJson.write(this.getGeometry(), writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    @JsonIgnore
    @Transient
    public Geometry getGeometry() {
        if (this.geometry == null) {
            setGeometry();
        }
        return this.geometry;
    }

    @JsonIgnore
    @Transient
    public Geometry setGeometry() {
        List<Geometry> precinctGeometries = new ArrayList<>(this.precincts.size());
        for (Precinct precinct : this.precincts) {
            Geometry geometry = precinct.getGeometry();
            precinctGeometries.add(geometry);
        }
        GeometryFactory GEOMETRY_FACTORY = JTSFactoryFinder.getGeometryFactory();

        if (precinctGeometries.size() > 1) {
            GeometryCollection geometryCollection = (GeometryCollection) GEOMETRY_FACTORY.buildGeometry(precinctGeometries);
            this.geometry = geometryCollection.union();
        } else {
            this.geometry = precinctGeometries.get(0);
        }
//        GeometryJSON geoJson = new GeometryJSON();
//        StringWriter writer = new StringWriter();
//        try {
//            geoJson.write(geometryCollection.union(), writer);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        return this.geometry;
    }

    @Transient
    public double getPerimeter() {
        if (perimeter == -1) {
            setPerimeter();
        }

        return perimeter;
    }

    @Transient
    public double setPerimeter() {
        double perimeter = 0;
        Coordinate coordinates[] = this.getGeometry().getCoordinates();

        for(int i = 0; i < coordinates.length - 1; i++) {
            double point1Long = coordinates[i].x;
            double point1Lat = coordinates[i].y;
            double point2Long = coordinates[i + 1].x;
            double point2Lat = coordinates[i + 1].y;
            perimeter += getDistanceBetweenCoordinates(point1Long, point1Lat, point2Long, point2Lat);
//            perimeter += coordinates[i].distance(coordinates[i+1]);
        }

        this.perimeter = perimeter;
        return this.perimeter;
    }

    @Transient
    public double getGeometryPerimeter() {
        return this.calcPerimeter(this.getGeometry());
    }

    @Transient
    public double getGeometryArea() {
        AreaFunction areaFunction = new AreaFunction();
        return this.calcArea(this.getGeometry());
    }

    /*
     * returns the distance between two coordinate points in km
     * formula taken from https://www.movable-type.co.uk/scripts/latlong.html
     */
    @Transient
    public double getDistanceBetweenCoordinates(double point1Long, double point1Lat, double point2Long, double point2Lat) {
        double R = 6371000;

        double lat = degreeToRadian(point2Lat - point1Lat);
        double lon = degreeToRadian(point2Long - point1Long);
        double a =
            Math.sin(lat / 2) * Math.sin(lat / 2) +
            Math.cos(degreeToRadian(point1Lat)) * Math.cos(degreeToRadian(point2Lat)) * Math.sin(lon/2) * Math.sin(lon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in km
        return d;
    }

    @Transient
    public double degreeToRadian(double degree) {
        return degree * (Math.PI/180);
    }

    @Override
    public String toString() {
        return this.name;
    }

    public double calcPerimeter(Geometry geometry) {
        Point centroid = geometry.getCentroid();
        try {
            String code = "AUTO:42001," + centroid.getX() + "," + centroid.getY();
            CoordinateReferenceSystem auto = null;
            try {
                auto = CRS.decode(code);
            } catch (FactoryException e) {
                e.printStackTrace();
            }

            MathTransform transform = CRS.findMathTransform(DefaultGeographicCRS.WGS84, auto);
            Geometry projed = null;
            try {
                projed = (Geometry) JTS.transform(geometry, transform);
            } catch (TransformException e) {
                e.printStackTrace();
            }
            double perimeter = this.getPerimeter();
            Measure measure = new Measure(perimeter, SI.METRE);
            return measure.doubleValue();
        } catch (MismatchedDimensionException | FactoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0.0;
    }

    public double calcArea(Geometry geometry) {
        Point centroid = geometry.getCentroid();
        try {
            String code = "AUTO:42001," + centroid.getX() + "," + centroid.getY();
            CoordinateReferenceSystem auto = null;
            try {
                auto = CRS.decode(code);
            } catch (FactoryException e) {
                e.printStackTrace();
            }

            MathTransform transform = CRS.findMathTransform(DefaultGeographicCRS.WGS84, auto);

            Geometry projed = null;
            try {
                projed = (Geometry) JTS.transform(geometry, transform);
            } catch (TransformException e) {
                e.printStackTrace();
            }
            Measure measure = new Measure(projed.getArea(), SI.SQUARE_METRE);
            return measure.doubleValue();
        } catch (MismatchedDimensionException | FactoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0.0;
    }

}
