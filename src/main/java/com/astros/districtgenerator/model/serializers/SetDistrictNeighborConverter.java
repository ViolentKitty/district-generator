package com.astros.districtgenerator.model.serializers;

import com.astros.districtgenerator.model.District;
import com.astros.districtgenerator.model.DistrictNeighbor;
import com.fasterxml.jackson.databind.util.StdConverter;

import java.util.Set;
import java.util.stream.Collectors;

public class SetDistrictNeighborConverter extends StdConverter<Set<DistrictNeighbor>, Set<Long>> {
    @Override
    public Set<Long> convert(Set<DistrictNeighbor> districtNeighbors) {
        return districtNeighbors.stream().map(districtNeighbor -> districtNeighbor.getNeighbor().getId()).collect(Collectors.toSet());
    }
}
