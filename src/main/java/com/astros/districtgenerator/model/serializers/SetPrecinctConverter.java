package com.astros.districtgenerator.model.serializers;

import com.astros.districtgenerator.model.District;
import com.astros.districtgenerator.model.Precinct;
import com.fasterxml.jackson.databind.util.StdConverter;

import java.util.Set;
import java.util.stream.Collectors;

public class SetPrecinctConverter extends StdConverter<Set<Precinct>, Set<String>> {
    @Override
    public Set<String> convert(Set<Precinct> precincts) {
        return precincts.stream().map(precinct -> precinct.getName()).collect(Collectors.toSet());
    }
}
