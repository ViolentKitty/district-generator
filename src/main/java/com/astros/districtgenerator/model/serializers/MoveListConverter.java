package com.astros.districtgenerator.model.serializers;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.astros.districtgenerator.algorithm.Move;
import com.astros.districtgenerator.model.Cluster;
import com.astros.districtgenerator.model.District;
import com.fasterxml.jackson.databind.util.StdConverter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class MoveListConverter extends StdConverter<List<Move>, List<Map<String, Long>>>{
    @Override
    public List<Map<String, Long>> convert(List<Move> moves){
        return moves.stream().map(move -> {
            Map<String,Long> response = new HashMap<>(3);
            response.put("precinct", move.getPrecinctToMove().getId());
            response.put("source", move.getSourceDistrict().getId());
            response.put("destination", move.getTargetDistrict().getId());
            return response;
        }).collect(Collectors.toList());
    }
}
