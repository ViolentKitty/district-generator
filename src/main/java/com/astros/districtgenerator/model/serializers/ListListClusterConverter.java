package com.astros.districtgenerator.model.serializers;

import com.astros.districtgenerator.model.Cluster;
import com.astros.districtgenerator.model.District;
import com.fasterxml.jackson.databind.util.StdConverter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ListListClusterConverter extends StdConverter<List<List<Cluster>>, List<Map<String, Long>>>{
    @Override
    public List<Map<String, Long>> convert(List<List<Cluster>> clusters){
        return clusters.stream().map(merge -> {
            Cluster source = merge.get(0);
            Cluster target = merge.get(1);
            Map<String, Long> merges = new HashMap<>();
            merges.put("source", source.getId());
            merges.put("target", target.getId());
            return merges;
        }).collect(Collectors.toList());
    }
}
