package com.astros.districtgenerator.model.serializers;

import com.astros.districtgenerator.model.District;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.util.StdConverter;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SetDistrictConverter extends StdConverter<Set<District>, Set<Long>> {
    @Override
    public Set<Long> convert(Set<District> districts) {
        return districts.stream().map(district -> district.getId()).collect(Collectors.toSet());
    }
}
