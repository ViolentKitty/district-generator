package com.astros.districtgenerator.model;

import com.astros.districtgenerator.model.auth.User;
import com.astros.districtgenerator.model.converters.SavedMapConverter;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "saved_map")
public class SavedMap {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//    @ManyToOne
//    private User user;
    @Column(name = "email")
    private String email;
    @Column(name = "precinct_to_district")
    @Convert(converter = SavedMapConverter.class)
    public Map<Long, Long> precinctToDistrict;

    public SavedMap() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<Long, Long> getPrecinctToDistrict() {
        return precinctToDistrict;
    }

    public void setPrecinctToDistrict(Map<Long, Long> precinctToDistrict) {
        this.precinctToDistrict = precinctToDistrict;
    }
}
