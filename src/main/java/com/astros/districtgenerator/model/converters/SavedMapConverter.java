package com.astros.districtgenerator.model.converters;

import javax.persistence.AttributeConverter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SavedMapConverter implements AttributeConverter<Map<Long, Long>, String> {
    @Override
    public String convertToDatabaseColumn(Map<Long, Long> map) {
        int mapSize = map.size();
        StringBuilder builder = new StringBuilder();
        int counter = 0;
        for (Map.Entry<Long, Long> entry : map.entrySet()) {
            counter++;
            if (counter == mapSize) {
                builder.append(entry.getValue() + " " + entry.getKey());
            } else {
                builder.append(entry.getValue() + " " + entry.getKey() + "-");
            }
        }
        return builder.toString();
    }

    @Override
    public Map<Long, Long> convertToEntityAttribute(String s) {
        String[] p2dMap = s.split("-");
        Map<Long, Long> map = new HashMap<>();
        for (String p2d : p2dMap) {
            String[] pd = p2d.split(" ");
            Long dId = Long.parseLong(pd[0]);
            Long pId = Long.parseLong(pd[1]);
            map.put(pId, dId);
        }
        return map;
    }
}
