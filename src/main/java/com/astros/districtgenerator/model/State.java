package com.astros.districtgenerator.model;

import com.astros.districtgenerator.model.serializers.SetDistrictConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.*;


@Data
@Entity
@Table(name = "state")
public class State {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "abbreviation")
    private String abbr;

    @Column(name = "area")
    private double area;


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "stateId")
    private Set<District> districts;

    @Column(name = "leaf_lat")
    private double latitude;

    @Column(name = "leaf_long")
    private double longitude;

    @Column(name = "leaf_zoom_level")
    private double zoomLevel;

    private static final Logger LOGGER = LoggerFactory.getLogger(State.class);

    public void printDistrictPerimeters() {
        for (District district : districts) {
            LOGGER.info("" + district.getGeometryPerimeter());
        }
    }

    @JsonIgnore
    @Transient
    public List<Precinct> getAllPrecincts() {
        List<Precinct> precincts = new ArrayList<>();
        for (District district : districts) {
            precincts.addAll(district.getPrecincts());
        }
        return precincts;
    }

    @Transient
    public District getDistrictById(int districtId) {
        return districts.stream()
                .filter(district -> district.getId() == districtId)
                .findFirst()
                .orElse(null);
    }

    @Transient
    public Precinct getPrecinctById(long precinctId) {
        List<Precinct> precincts = this.getAllPrecincts();
        return precincts.stream()
                .filter(precinct -> precinct.getId() == precinctId)
                .findFirst()
                .orElse(null);
    }

    public State() {
        this.districts = new HashSet<>();
    }

    public State(String name, String abbr, double area, Set<District> districts, double latitude, double longitude, double zoomLevel) {
        this.name = name;
        this.abbr = abbr;
        this.area = area;
        this.districts = districts;
        this.latitude = latitude;
        this.longitude = longitude;
        this.zoomLevel = zoomLevel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public Set<District> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getZoomLevel() {
        return zoomLevel;
    }

    public void setZoomLevel(double zoomLevel) {
        this.zoomLevel = zoomLevel;
    }
}
