package com.astros.districtgenerator.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "precinct_neighbor")
public class PrecinctNeighbor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Precinct owner;

    @OneToOne
    @JoinColumn(name = "neighbor_id")
    private Precinct neighbor;

    @Column(name = "county_joinability")
    private double countyJoinability;

    @Column(name = "demographic_joinability")
    private double demographicJoinability;

    public PrecinctNeighbor() {}

    public PrecinctNeighbor(Precinct owner, Precinct neighbor, double countyJoinability, double demographicJoinability) {
        this();
        this.owner = owner;
        this.neighbor = neighbor;
        this.countyJoinability = countyJoinability;
        this.demographicJoinability = demographicJoinability;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Precinct getOwner() {
        return owner;
    }

    public void setOwner(Precinct owner) {
        this.owner = owner;
    }

    public Precinct getNeighbor() {
        return neighbor;
    }

    public void setNeighbor(Precinct neighbor) {
        this.neighbor = neighbor;
    }

    public double getCountyJoinability() {
        return countyJoinability;
    }

    public void setCountyJoinability(double countyJoinability) {
        this.countyJoinability = countyJoinability;
    }

    public double getDemographicJoinability() {
        return demographicJoinability;
    }

    public void setDemographicJoinability(double demographicJoinability) {
        this.demographicJoinability = demographicJoinability;
    }
}
