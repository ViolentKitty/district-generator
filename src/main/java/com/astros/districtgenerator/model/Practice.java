package com.astros.districtgenerator.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "Practice")
public class Practice {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

}
