package com.astros.districtgenerator.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "DistrictNeighbor")
public class DistrictNeighbor {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;


    @OneToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private District owner;

    @OneToOne
    @JoinColumn(name = "neighbor_id", referencedColumnName = "id")
    private District neighbor;

    public DistrictNeighbor() {}

    public DistrictNeighbor(District owner, District neighbor) {
        this();
        this.owner = owner;
        this.neighbor = neighbor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public District getOwner() {
        return owner;
    }

    public void setOwner(District owner) {
        this.owner = owner;
    }

    public District getNeighbor() {
        return neighbor;
    }

    public void setNeighbor(District neighbor) {
        this.neighbor = neighbor;
    }
}
