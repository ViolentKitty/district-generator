package com.astros.districtgenerator.algorithm;

import com.astros.districtgenerator.model.*;
import com.astros.districtgenerator.repository.DistrictRepository;
import com.astros.districtgenerator.repository.PrecinctRepository;
import com.astros.districtgenerator.repository.SavedMapRepository;
import com.astros.districtgenerator.repository.StateRepository;
import org.locationtech.jts.algorithm.MinimumBoundingCircle;
import org.locationtech.jts.geom.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class Algorithm {

    private State state;
    private UserPreferences userPreferences;
    private List<Cluster> clusters;
    private Map<District, Double> districtScores;
    private int partitionMoveCount;
    private int redistrictMoveCount;
    private List<List<Cluster>> partitionMoves;
    private List<Move> redistrictMoves;
    private int selectedState;

    @Autowired
    private StateRepository stateRepository;
    @Autowired
    private PrecinctRepository precinctRepository;
    private DistrictRepository districtRepository;
    @Autowired
    private SavedMapRepository savedMapRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(Algorithm.class);

    public void resetAlgorithmState() {
        this.initializeState(selectedState);
        partitionMoveCount = 0;
        redistrictMoveCount = 0;
        clusters = new CopyOnWriteArrayList<>();
        districtScores = new HashMap<>();
        partitionMoves = new ArrayList<>();
        redistrictMoves = new ArrayList<>();
    }

    public Algorithm() {
        partitionMoveCount = 0;
        redistrictMoveCount = 0;
        clusters = new CopyOnWriteArrayList<>();
        districtScores = new HashMap<>();
        partitionMoves = new ArrayList<>();
        redistrictMoves = new ArrayList<>();
    }

    public BatchResult runSingleBatch(int runNum, int requestedRunNum) {
        LOGGER.info("num districts "+userPreferences.getNumDistricts());
        for (Measure measure : userPreferences.getMeasures()) {
            if (measure.isChange()) {
                // we want to get the step size for that measure which is dependant on the base measure value
                double newWeightVal = ((1.0 - measure.getOriginalWeightVal() ) / requestedRunNum) * runNum;
                measure.setWeight(newWeightVal);
            }
        }
        while (partition() != null) {}
        while (redistrict() != null) {}
        BatchResult batchResult = new BatchResult(state, districtScores, userPreferences, partitionMoveCount, redistrictMoveCount);
        batchResult.setObjectiveFunctionValue(this.getObjectiveFunctionValue());
        batchResult.setPartitionMoves(partitionMoves);
        batchResult.setRedistrictMoves(redistrictMoves);
        resetAlgorithmState();
        return batchResult;
    }



    public List<Cluster> partition() {
        if (this.clusters.size() == 0) {
            LOGGER.info("Algorithm -- initializing clusters");
            this.clusters = this.initializeClusters();
        }
        if (this.clusters.size() == userPreferences.getNumDistricts()) {
            return null;
        }
        List<Cluster> clusterPair = this.determineCandidatePairs();
        partitionMoves.add(clusterPair);
        LOGGER.info("Joining cluster " + clusterPair.get(0).getId() + " " + clusterPair.get(1).getId());
        clusterPair.get(0).absorbCluster(clusterPair.get(1));
        clusters.remove(clusterPair.get(1));
        partitionMoveCount++;
        LOGGER.info("Num Clusters: " + clusters.size() + " " + partitionMoveCount);
        return clusterPair;
    }

    // phase 2
    public Move redistrict() {
        if (redistrictMoveCount == 0) {
            test_initUserPreferences();
//            LOGGER.info("redistrict() first call - init'in district scores");
            this.getState().setDistricts(initializeDistricts());
            initDistrictScores();
        }
        District sourceDistrict = this.getWorstDistrict();
        for (Precinct borderPrecinct : sourceDistrict.getBorderPrecincts()) {
            District targetDistrict = sourceDistrict.getNeighborDistrict(borderPrecinct);
            Move currMove = new Move(sourceDistrict, targetDistrict, borderPrecinct);
            currMove.execute();
//            LOGGER.info("executing move -> precinct: " + borderPrecinct.getId() + " from district: " + sourceDistrict.getId() + " to district: " + targetDistrict.getId());
            if (keepMove(sourceDistrict, targetDistrict)) {
//                LOGGER.info("keeping last move");
                districtScores.put(sourceDistrict, this.calculateDistrictScore(sourceDistrict));
                districtScores.put(targetDistrict, this.calculateDistrictScore(targetDistrict));
                redistrictMoveCount++;
                getObjectiveFunctionValue();
                redistrictMoves.add(currMove);
                return currMove;
            } else {
                LOGGER.info("Unexecuting move");
                currMove.unexecute();
            }
        }
        LOGGER.warn("no good move was found - exiting redistrict()");
        return null;
    }

    private double getObjectiveFunctionValue() {
        StringBuilder stringBuilder = new StringBuilder();
        double totalScore = 0;
        for (Map.Entry<District, Double> score : districtScores.entrySet()) {
//            LOGGER.info(score.getKey().getId() + " - " + score.getValue());
            totalScore += score.getValue();
        }
        LOGGER.info("objective function value: " + totalScore / districtScores.size());
        return totalScore/districtScores.size();
    }

    private District getWorstDistrict() {
        return Collections.min(this.getState().getDistricts(),
                (d1, d2) -> Double.compare(this.districtScores.get(d1), this.districtScores.get(d2)));
    }

    private List<Cluster> initializeClusters() {
        List<Precinct> precincts = this.state.getAllPrecincts();
        List<Cluster> clusters = new ArrayList<>(precincts.size());
        Map<Long, Cluster> clusterMap = new HashMap<>();
        for (Precinct p : precincts){
            Cluster c = new Cluster(p);
            clusters.add(c);
            clusterMap.put(c.getId(), c);
        }
        int num_edges = 0;
        for (Precinct p : precincts){
            for (PrecinctNeighbor pn : p.getNeighbors()){
                num_edges++;
                Cluster owner = clusterMap.get(pn.getOwner().getId());
                Cluster neighbor = clusterMap.get(pn.getNeighbor().getId());
                ClusterNeighbor cn = new ClusterNeighbor(owner, neighbor, pn.getCountyJoinability(), pn.getDemographicJoinability());
                owner.getNeighbors().add(cn);
            }
        }

        return clusters;
    }

    public Set<District> initializeDistricts() {
        List<Cluster> clusters = this.getClusters();
        Set<District> districts = new HashSet<>(clusters.size());
        Map<Long, District> districtMap = new HashMap<>();
        for (Cluster cluster : clusters){
            District district = new District(cluster);
            districts.add(district);
            districtMap.put(district.getId(), district);
        }

        int num_edges = 0;
        for (Cluster cluster : clusters){
            for (ClusterNeighbor cn : cluster.getNeighbors()){
                num_edges++;
                District owner = districtMap.get(cn.getOwner().getId());
                District neighbor = districtMap.get(cn.getNeighbor().getId());
                DistrictNeighbor dn = new DistrictNeighbor(owner, neighbor);
                owner.getNeighbors().add(dn);
            }
        }

        return districts;
    }

    private boolean keepMove(District sourceDistrict, District targetDistrict) {
        double sourceDistrictNewScore = this.calculateDistrictScore(sourceDistrict);
        double targetDistrictNewScore = this.calculateDistrictScore(targetDistrict);
        return sourceDistrictNewScore + targetDistrictNewScore > districtScores.get(sourceDistrict) + districtScores.get(targetDistrict);
    }

    private void initDistrictScores() {
        for (District district : this.getState().getDistricts()) {
            double districtScore = this.calculateDistrictScore(district);
            districtScores.put(district, districtScore);
        }
    }

    public List<Cluster> determineCandidatePairs() {
        Cluster source = this.clusters.get(0);
        for (Cluster c : this.clusters){
            if (c.getDemographic().getTotalPopulation() < source.getDemographic().getTotalPopulation()){
                source = c;
            }
        }

//        LOGGER.info("" + source.getId() + " " + source.getNeighbors().size());
        ClusterNeighbor target = (ClusterNeighbor) source.getNeighbors().toArray()[0];
        for (ClusterNeighbor cn : source.getNeighbors()){
            if (cn.getTotalJoinability() > target.getTotalJoinability()){
                target = cn;
            }
        }
//        Cluster source = Collections.min(this.clusters,
//                (c1, c2) -> c1.getDemographic().getTotalPopulation() - c2.getDemographic().getTotalPopulation());
//        Cluster target = Collections.max(source.getNeighbors(),
//                (ce1, ce2) -> Double.compare(ce1.getTotalJoinability(), ce2.getTotalJoinability()))
//                .getNeighbor();

        List<Cluster> clusterPair = new ArrayList<>(2);
        clusterPair.add(source);
        clusterPair.add(target.getNeighbor());
        return clusterPair;
    }

    // TODO: implement
    public State initializeState(int selectedState) {
        this.selectedState = selectedState;
        Optional<State> optionalState = stateRepository.findById(selectedState);
        State state = null;
        if (optionalState.isPresent()) {
            this.state = optionalState.get();
            return state;
        } else {
            return null;
        }
    }

    public List<Precinct> getPrecinctsByState(int selectedState){
        return precinctRepository.findAllByStateId(selectedState);
    }

    public boolean saveCurrentState() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return false;
        }

        Map<Long, Long> p2d = new HashMap<>();
        for (Precinct precinct : this.getState().getAllPrecincts()) {
            p2d.put(precinct.getId(), precinct.getDistrict().getId());
        }

        SavedMap savedMap = new SavedMap();
        savedMap.setEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        savedMap.setPrecinctToDistrict(p2d);
        savedMapRepository.save(savedMap);
        return true;
    }

    public double calculateDistrictScore(District district) {
        double districtScore = 0;
        double numMeasures = 0;
        double score;

        for (Measure measure : this.userPreferences.getMeasures()) {
            switch (measure.getMeasureType()) {
                case SCHWARZBERG_COMPACTNESS:
                    numMeasures++;
                    score = this.measureSchwarzbergCompactness(district);
//                    LOGGER.info("schwarzberg score = " + score);
                    districtScore += score * measure.getWeight();
                    break;
                case POLSBY_POPPER_COMPACTNESS:
                    numMeasures++;
                    score = this.measurePolsbyPopperCompactness(district);
//                    LOGGER.info("polsby popper score = " + score);
                    districtScore += score * measure.getWeight();
                    break;
                case REOCK_COMPACTNESS:
                    numMeasures++;
                    score = this.measureReockCompactness(district);
//                    LOGGER.info("reock score = " + score);
                    districtScore += score * measure.getWeight();
                    break;
                case CONVEX_HULL_COMPACTNESS:
                    numMeasures++;
                    score = this.measureConvexHullCompactness(district);
//                    LOGGER.info("convexhull score = " + score);
                    districtScore += score * measure.getWeight();
                    break;
                case EFFICIENCY_GAP:
                    numMeasures++;
                    score = this.measureEfficiencyGap(district);
                    LOGGER.info("efficiency gap score = " + score);
                    districtScore += score * measure.getWeight();
                    break;
                case MEAN_MEDIAN:
                    numMeasures++;
                    score = this.measureMeanMedian(district);
                    LOGGER.info("meanmedian score = " + score);
                    districtScore += score * measure.getWeight();
                    break;
                case EQUAL_POPULATION:
                    numMeasures++;
                    score = this.measureEqualPopulation(district);
                    LOGGER.info("equal pop score = " + score);
                    districtScore += score * measure.getWeight();
                    break;
                default:
                    LOGGER.error("unknown measure used");
            }
        }

        double normalizedScore = districtScore / numMeasures;
        return normalizedScore;
    }

    private double measureReockCompactness(District district) {
        double districtArea = district.getGeometryArea();
        MinimumBoundingCircle mbc = new MinimumBoundingCircle(district.getGeometry());
        double mbcArea = district.calcArea(mbc.getCircle()) ;
        return districtArea / mbcArea;
    }

    private double measurePolsbyPopperCompactness(District district) {
        double area = district.getGeometryArea();
        double perimeter = district.getPerimeter();
        double score = (4 * Math.PI) * (area / Math.pow(perimeter, 2));
        return score;
    }

    private double measureConvexHullCompactness(District district) {
        double area = district.getGeometryArea();
        Geometry convexHull = district.getGeometry().convexHull();
        double convexHullArea = district.calcArea(convexHull);
        return area / convexHullArea;
    }

    private double measureSchwarzbergCompactness(District district) {
        double area = district.getGeometryArea();
        double perimeter = district.getPerimeter();
        double score = ((2 * Math.PI) * (Math.sqrt(area / Math.PI))) / (perimeter);
        return score;
    }

    private double measureEfficiencyGap(District district) {
        double wastedDemVotes = 0;
        double wastedRepVotes = 0;
        double totalVotes = 0;

        for (Precinct precinct : district.getPrecincts()) {
            int demCount = precinct.getElection().getDemocratCount();
            int repCount = precinct.getElection().getRepublicCount();
            int totalPrecinctVotes = demCount + repCount;

            int votesRequiredToWin = (totalPrecinctVotes / 2) + 1;
            if (demCount == repCount) {
            }
            else if (demCount > repCount) {
                wastedDemVotes += demCount - votesRequiredToWin;
                wastedRepVotes += repCount;
            } else {
                wastedRepVotes += repCount - votesRequiredToWin;
                wastedDemVotes += demCount;
            }

            totalVotes += totalPrecinctVotes;
        }

        return 1 - Math.abs(wastedDemVotes - wastedRepVotes) / totalVotes;
    }

    private double measureEqualPopulation(District district) {
        int totalPopulation = 0;
        for (District d : this.getState().getDistricts()) {
            totalPopulation += d.getDemographic().getTotalPopulation();
        }
        int numDistricts = this.getState().getDistricts().size();
        int idealPopulation = totalPopulation / numDistricts;

        double errorSum = 0;
        for (District d : this.getState().getDistricts()) {
            errorSum += Math.pow(idealPopulation - d.getDemographic().getTotalPopulation(), 2);
        }
        errorSum /= numDistricts;

        if(errorSum == 0){
            return 1.0;
        }

        return (1 / errorSum);
    }

    private double measureMeanMedian(District district) {
        int numPrecincts = district.getPrecincts().size();
        List<Integer> demVoteList = new ArrayList<>(numPrecincts);
        List<Integer> repVoteList = new ArrayList<>(numPrecincts);
        int demCount = 0;
        int repCount = 0;

        for (Precinct precinct : district.getPrecincts()) {
            demVoteList.add(precinct.getElection().getDemocratCount());
            demCount += precinct.getElection().getDemocratCount();
            repVoteList.add(precinct.getElection().getRepublicCount());
            repCount += precinct.getElection().getRepublicCount();
        }

        Collections.sort(demVoteList);
        Collections.sort(repVoteList);
        int middleIndex = numPrecincts / 2;
//        if (numPrecincts % 2 == 1) {
//            middleIndex++;
//        }

        double demMean = demCount / numPrecincts;
        double repMean = repCount / numPrecincts;
        int demMedian = demVoteList.get(middleIndex);
        int repMedian = repVoteList.get(middleIndex);

        double demScore = 1 / (Math.abs(demMean - demMedian));
        double repScore = 1 / (Math.abs(repMean - repMedian));
        return (demScore + repScore) / 2;
    }

    public void test_initUserPreferences() {
        userPreferences = new UserPreferences();
        userPreferences
                .setNumDistricts(this.userPreferences.getNumDistricts())
                .setNumRuns(1);
        Measure polsby = new Measure();
        polsby
                .setMeasureType(MeasureType.MEAN_MEDIAN)
                .setWeight(0.5)
                .setChange(true);
        userPreferences.addMeasure(polsby);
        Measure reock = new Measure();
        reock
                .setMeasureType(MeasureType.EFFICIENCY_GAP)
                .setWeight(0.5)
                .setChange(true);
        userPreferences.addMeasure(reock);
        Measure convexHull = new Measure();
        convexHull
                .setMeasureType(MeasureType.EQUAL_POPULATION)
                .setWeight(0.5)
                .setChange(true);
        userPreferences.addMeasure(convexHull);
        this.setUserPreferences(userPreferences);
    }

    // TODO: implement
    private double measureRacialFairness() {
        return 0;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public UserPreferences getUserPreferences() {
        return userPreferences;
    }

    public void setUserPreferences(UserPreferences userPreferences) {
        this.userPreferences = userPreferences;
    }

    public List<Cluster> getClusters() {
        return clusters;
    }

    public void setClusters(List<Cluster> clusters) {
        this.clusters = clusters;
    }

    public Map<District, Double> getDistrictScores() {
        return districtScores;
    }

    public void setDistrictScores(Map<District, Double> districtScores) {
        this.districtScores = districtScores;
    }

    public List<List<Cluster>> getPartitionMoves() {
        return partitionMoves;
    }

    public void setPartitionMoves(List<List<Cluster>> partitionMoves) {
        this.partitionMoves = partitionMoves;
    }

    public List<Move> getRedistrictMoves() {
        return redistrictMoves;
    }

    public void setRedistrictMoves(List<Move> redistrictMoves) {
        this.redistrictMoves = redistrictMoves;
    }

    public int getPartitionMoveCount() {
        return partitionMoveCount;
    }

    public void setPartitionMoveCount(int partitionMoveCount) {
        this.partitionMoveCount = partitionMoveCount;
    }

    public int getRedistrictMoveCount() {
        return redistrictMoveCount;
    }

    public void setRedistrictMoveCount(int redistrictMoveCount) {
        this.redistrictMoveCount = redistrictMoveCount;
    }

}
