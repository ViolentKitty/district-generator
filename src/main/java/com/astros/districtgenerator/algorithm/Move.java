package com.astros.districtgenerator.algorithm;

import com.astros.districtgenerator.model.District;
import com.astros.districtgenerator.model.Precinct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Move {

    private District sourceDistrict;
    private District targetDistrict;
    private Precinct precinctToMove;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public Move() {}

    public Move(District sourceDistrict, District targetDistrict, Precinct precinctToMove) {
        this.sourceDistrict = sourceDistrict;
        this.targetDistrict = targetDistrict;
        this.precinctToMove = precinctToMove;
    }

    public District getSourceDistrict() {
        return sourceDistrict;
    }

    public Move setSourceDistrict(District sourceDistrict) {
        this.sourceDistrict = sourceDistrict;
        return this;
    }

    public District getTargetDistrict() {
        return targetDistrict;
    }

    public Move setTargetDistrict(District targetDistrict) {
        this.targetDistrict = targetDistrict;
        return this;
    }

    public Precinct getPrecinctToMove() {
        return precinctToMove;
    }

    public Move setPrecinctToMove(Precinct precinctToMove) {
        this.precinctToMove = precinctToMove;
        return this;
    }

    public void execute() {
        sourceDistrict.removePrecinct(precinctToMove);
        targetDistrict.addPrecinct(precinctToMove);
        precinctToMove.setDistrict(targetDistrict);
    }

    public void unexecute() {
        sourceDistrict.addPrecinct(precinctToMove);
        targetDistrict.removePrecinct(precinctToMove);
        precinctToMove.setDistrict(sourceDistrict);
    }

}
