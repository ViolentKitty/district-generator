package com.astros.districtgenerator.algorithm;

public enum MeasureType {
    REOCK_COMPACTNESS,
    SCHWARZBERG_COMPACTNESS,
    POLSBY_POPPER_COMPACTNESS,
    CONVEX_HULL_COMPACTNESS,
    EFFICIENCY_GAP,
    MEAN_MEDIAN,
    EQUAL_POPULATION
}
