package com.astros.districtgenerator.algorithm;

import com.astros.districtgenerator.model.Cluster;
import com.astros.districtgenerator.model.District;
import com.astros.districtgenerator.model.State;
import com.astros.districtgenerator.model.serializers.ListListClusterConverter;
import com.astros.districtgenerator.model.serializers.MoveListConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.*;

// TODO: not implemented yet
public class BatchResult {

    private State state;
    private Map<District, Double> districtScores;
    private UserPreferences userPreferences;
    private int partitionMoveCount;
    private int redistrictMoveCount;
    private double objectiveFunctionValue;
    @JsonSerialize(converter = ListListClusterConverter.class)
    private List<List<Cluster>> partitionMoves;
    @JsonSerialize(converter = MoveListConverter.class)
    private List<Move> redistrictMoves;

    public BatchResult() {
        districtScores = new HashMap<>();
        partitionMoveCount = 0;
        redistrictMoveCount = 0;
    }

    public BatchResult(State state, Map<District, Double> districtScores, UserPreferences userPreferences,
                       int partitionMoveCount, int redistrictMoveCount) {
        this();
        this.state = state;
        this.districtScores = districtScores;
        this.userPreferences = userPreferences;
        this.partitionMoveCount = partitionMoveCount;
        this.redistrictMoveCount = redistrictMoveCount;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Map<District, Double> getDistrictScores() {
        return districtScores;
    }

    public void setDistrictScores(Map<District, Double> districtScores) {
        this.districtScores = districtScores;
    }

    public UserPreferences getUserPreferences() {
        return userPreferences;
    }

    public void setUserPreferences(UserPreferences userPreferences) {
        this.userPreferences = userPreferences;
    }

    public int getPartitionMoveCount() {
        return partitionMoveCount;
    }

    public void setPartitionMoveCount(int partitionMoveCount) {
        this.partitionMoveCount = partitionMoveCount;
    }

    public int getRedistrictMoveCount() {
        return redistrictMoveCount;
    }

    public void setRedistrictMoveCount(int redistrictMoveCount) {
        this.redistrictMoveCount = redistrictMoveCount;
    }

    public double getObjectiveFunctionValue() {
        return objectiveFunctionValue;
    }

    public void setObjectiveFunctionValue(double objectiveFunctionValue) {
        this.objectiveFunctionValue = objectiveFunctionValue;
    }

    public List<List<Cluster>> getPartitionMoves() {
        return partitionMoves;
    }

    public void setPartitionMoves(List<List<Cluster>> partitionMoves) {
        this.partitionMoves = partitionMoves;
    }

    public List<Move> getRedistrictMoves() {
        return redistrictMoves;
    }

    public void setRedistrictMoves(List<Move> redistrictMoves) {
        this.redistrictMoves = redistrictMoves;
    }
}