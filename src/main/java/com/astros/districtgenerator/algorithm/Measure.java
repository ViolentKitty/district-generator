package com.astros.districtgenerator.algorithm;

public class Measure {

    private MeasureType measureType;
    private double weight;
    private double originalWeightVal;
    private boolean change; // used for batch runs

    public Measure() {}

    public Measure(MeasureType measureType, double weight, boolean change) {
        this.measureType = measureType;
        this.originalWeightVal = weight;
        this.weight = weight;
        this.change = change;
    }

    public MeasureType getMeasureType() {
        return measureType;
    }

    public Measure setMeasureType(MeasureType measureType) {
        this.measureType = measureType;
        return this;
    }

    public double getOriginalWeightVal() {
        return originalWeightVal;
    }

    public void setOriginalWeightVal(double originalWeightVal) {
        this.originalWeightVal = originalWeightVal;
    }

    public double getWeight() {
        return weight;
    }

    public Measure setWeight(double weight) {
        this.weight = weight;
        return this;
    }

    public boolean isChange() {
        return change;
    }

    public Measure setChange(boolean change) {
        this.change = change;
        return this;
    }

    public void randomizeWeight() {
        this.weight = Math.random();
    }

    @Override
    public String toString() {
        return "measureType: " + this.measureType + ", weight: " + this.weight + ", change: " + this.change;
    }

}
