package com.astros.districtgenerator.algorithm;

import com.astros.districtgenerator.model.State;
import com.astros.districtgenerator.model.auth.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserPreferences {

    private String stateName;
    private int numDistricts;
    private int numRuns;
    private boolean updateStatus;
    private List<Measure> measures;

    public UserPreferences() {
        measures = new ArrayList<>();
    }

    public String getStateName() { return stateName; }

    public UserPreferences(String stateName, int numDistricts, int numRuns, boolean updateStatus, List<Measure> measures) {
        this();
        this.stateName = stateName;
        this.numDistricts = numDistricts;
        this.numRuns = numRuns;
        this.updateStatus = updateStatus;
        this.measures = measures;
    }

    public UserPreferences(UserPreferences o) {
        this(o.stateName, o.numDistricts, o.numRuns, o.updateStatus, o.measures.stream().collect(Collectors.toList()));
    }

    public UserPreferences setStateName(String stateName) {
        this.stateName = stateName;
        return this;
    }

    public int getNumDistricts() {
        return numDistricts;
    }

    public UserPreferences setNumDistricts(int numDistricts) {
        this.numDistricts = numDistricts;
        return this;
    }

    public int getNumRuns() {
        return numRuns;
    }

    public UserPreferences setNumRuns(int numRuns) {
        this.numRuns = numRuns;
        return this;
    }

    public boolean getUpdateStatus() { return updateStatus; }

    public UserPreferences setUpdateStatus(boolean updateStatus) {
        this.updateStatus = updateStatus;
        return this;
    }

    public List<Measure> getMeasures() {
        return measures;
    }

    public UserPreferences setMeasures(List<Measure> measures) {
        this.measures = measures;
        return this;
    }

    public void addMeasure(Measure measure) {
        measures.add(measure);
    }

    public void toggleUpdateStatus() {
        this.updateStatus = !this.updateStatus;
    }

}
