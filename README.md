# Congressional District Generation
## States of choice:
    Texas
    Hawaii
    Missouri
---
## TODO:
- Develop front end page (no logic)
  - Due 3/10 
- Develop the use cases (sequence/class diagrams)
  - Due 3/17 (One sequence and one class)
  - We can present this like the first group in class
  - Is just supposed to show our approach for the diagrams
- Get the data for each of the states and figure out what preprocessing needs to be done. 
  - When it is done it can be linked to the repo, might have to put it on a dropbox or google drive if the dataset is too big.
---
## Contributors: 
    Aakash Kattelu
    Kevin Tran
    MD Hossain
    Ishan Sethi
